from django.conf.urls import patterns, include, url
from myproject.forms import *

from registration.forms import RegistrationFormUniqueEmail

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# moderation
# from moderation.helpers import auto_discover as moderation_auto_discover
# moderation_auto_discover()




# Examples:
# url(r'^$', 'myproject.views.home', name='home'),
# url(r'^myproject/', include('myproject.foo.urls')),
urlpatterns = patterns('',
	# Uncomment the admin/doc line below to enable admin documentation:
	url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),

	(r'^grappelli/', include('grappelli.urls')),


	url(r'^profile/register/$', 'registration.views.register',
		{'form_class': RegistrationFormUniqueEmail,
		 'backend': 'registration.backends.default.DefaultBackend'},
		 name='registration_register'),

	#django-registration
	(r'^profile/', include('registration.backends.default.urls'), {"extra_context": {}}),
)

#Statics
urlpatterns += patterns('myproject.views',
	url(r'^(terms)/$', 'static_pages', {"extra_context": {}}, name='terms'),
	url(r'^(about)/$', 'static_pages', {"extra_context": {}}, name='aboutus'),
	url(r'^(disclaimer)/$', 'static_pages', {"extra_context": {}}, name='disclaimer'),
	url(r'^(privacypolicy)/$', 'static_pages', {"extra_context": {}}, name='privacy'),
	url(r'^(sponsorship)/$', 'static_pages', {"extra_context": {}}, name='sponsorship'),
	url(r'^(contactus)/$', 'static_pages', {"extra_context": {}}, name='contactus'),
)



# Homepage
urlpatterns += patterns('myproject.views',
	url(r'^$', 'homepage', {"extra_context": {}}, name='homepage'),
)

urlpatterns += patterns('apps.authentication.views',
	url(r'^recovery/', 'recovery'),
)

# Search Results page
urlpatterns += patterns('',
	url(r'^search/', include('apps.search.urls'), {"extra_context": {}}),
)

# Company profiles
urlpatterns += patterns('',
	url(r'^company/', include('apps.company_profile_edit.urls'), {"extra_context": {}}),
)

# User profiles
urlpatterns += patterns('',
	url(r'^users/', include('apps.authentication.urls'), {"extra_context": {}}),
)

# Ads
urlpatterns += patterns('',
	url(r'^msadtrackr/', include('apps.ad_interface.urls'), {"extra_context": {}}),
)


# Admin tools
urlpatterns += patterns('',
	url(r'^admintools/', include('apps.admintools.urls'), {"extra_context": {}}),
)



# ... serving media on local server
from django.conf import settings

if settings.LOCAL_SERVER:
	# user uploaded files (images, css, javascript, etc.)
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))

