from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from apps.search.models import *
from apps.search.forms import *
from apps.ad_interface.models import *

@csrf_protect
def static_pages(request, page, extra_context={}):

	title = ""
	template = ""
	
	if page == "terms":
		title = "Terms and Conditions"
		template = "terms.html"

	if page == "about":
		title = "About Us"
		template = "aboutus.html"

	if page == "contactus":
		title = "Contact Us"
		template = "contact.html"

	if page == "disclaimer":
		title = "Financial Information Disclaimer"
		template = "disclaimer.html"

	if page == "sponsorship":
		title = "Sponsorship Opportunities"
		template = "sponsorship.html"

	if page == "privacypolicy":
		title = "Privacy Policy"
		template = "privacy.html"

	args = {"title":title}

	return render_to_response(template, args, context_instance=RequestContext(request))



	
@csrf_protect
def homepage(request, extra_context={}):

	ads_dict = {'top_banner_ads':multi_ad_picker(1, 3), 'side_ad':single_ad_picker(2)}

	

	clients = Company.objects.filter(company_level__gt = 0)[:10]
	rss = []
	for client in clients:
		rss.append(client.rss_news)
	
	# commodity snapshot
	# commo_snap_list = []
	# for commo in Commodity.objects.all():
	# 	if commo.feed_ticker_symbol:
	# 		commo_snap_list.append(commo)

	#commo_snap_list[0], commo_snap_list[1], commo_snap_list[2], commo_snap_list[3], commo_snap_list[4] = commo_snap_list[2], commo_snap_list[4], commo_snap_list[0], commo_snap_list[3], commo_snap_list[2]
	gold = Commodity.objects.get(commodity_name="Gold")
	silver = Commodity.objects.get(commodity_name="Silver")
	copper = Commodity.objects.get(commodity_name="Copper")
	oil = Commodity.objects.get(commodity_name="Oil")
	gas = Commodity.objects.get(commodity_name="Natural gas")

	commo_snap_list = [gold, silver, copper, oil, gas]

	# market snapshot
	stock_snap_list = []
	for stock in StockMarket.objects.all():
		if stock.feed_ticker_symbol and stock.feed_ticker_symbol != 'TSX-V':
			stock_snap_list.append(stock)

	stock_snap_list[1], stock_snap_list[2] = stock_snap_list[2], stock_snap_list[1]

	# top followed companies
	top_comps = []
	for top_comp in Company.objects.order_by("tracked"):
		if top_comp.tracked.count() == 0:
			break
		else:
			top_comps.append(top_comp)

	top_comps = list(set(top_comps))
	top_comps.sort(key=lambda x: x.tracked.count(), reverse=True)


	if request.user.is_authenticated():
		fin_form = FinancialInfoSearchForm()
	else:
		fin_form = FinancialInfoDisabledForm()

	forms_dict = {'mainbox_fin_search': fin_form, "mainbox_prop_search": PropertySearchForm}
	select2_dict = {'commodity_list': Commodity.objects.all(), 'regions': Region.objects.all(), 'countries': Country.objects.all(), 'states': State.objects.all(), 'commodities': Commodity.objects.all()}

	args = {"title":"A Junior Resource Search Engine", 'commo_snap_list':commo_snap_list,
	 'stock_snap_list':stock_snap_list, 'top_comps':top_comps[:5], 'rss':rss,}
	args.update(extra_context)
	args.update(forms_dict)
	args.update(select2_dict)
	args.update(ads_dict)
	return render_to_response('index.html', args, context_instance=RequestContext(request))
