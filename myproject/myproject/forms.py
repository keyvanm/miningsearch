from django import forms

class NavBarSearch(forms.Form):
	wg = forms.TextInput(attrs={'placeholder':'Search for ticker symbols or names',\
								'class':'span4', 'id':'', 'type': 'search'})
	q = forms.CharField(widget=wg)

# class RegisterField(forms.Form):
# 	pass