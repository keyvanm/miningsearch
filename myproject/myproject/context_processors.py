import reversion
from myproject.forms import *


def get_current_url(request):
	return {
	   'CURRENT_URL': request.get_full_path(),
	   'REQUEST': request,
	    'NUM_OF_PENDING': reversion.models.Revision.objects.filter(versionapprove__is_approved=0).count(),
	   'NAVBAR_SEARCH': NavBarSearch,
	}