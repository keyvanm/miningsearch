from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from myproject.forms import NavBarSearch

from models import *
from apps.search.forms import *
from django.shortcuts import render_to_response

from haystack.query import SearchQuerySet, EmptySearchQuerySet, SQ


from apps.ad_interface.models import *


select2_dict = {'regions': Region.objects.all(), 'countries': Country.objects.all(), 'states': State.objects.all(), 'commodities': Commodity.objects.all()}


# def status_decoder(coded):
# 	if not coded: return set([''])
# 	ret_set = set()
# 	rec_set = status_decoder(coded[1:])
# 	for item in rec_set:
# 		ret_set.update([coded[0]+item, '_'+item])
# 	return ret_set


def financial_info_search(sqs, fin_form):
	fin_form_clean = fin_form.cleaned_data
	price_min = fin_form_clean.get('price_min')
	price_max = fin_form_clean.get('price_max')
	vol_min = fin_form_clean.get('vol_min')
	vol_max = fin_form_clean.get('vol_max')
	marketcap_min = fin_form_clean.get('marketcap_min')
	marketcap_max = fin_form_clean.get('marketcap_max')
	shouts_min = fin_form_clean.get('shouts_min')
	shouts_max = fin_form_clean.get('shouts_max')
	fifty_low_min = fin_form_clean.get('fifty_low_min')
	fifty_high_max = fin_form_clean.get('fifty_high_max')

	try:
		price_min = int(10000*price_min)
	except: pass
	try:
		price_max = int(10000*price_max)
	except: pass

	try:
		fifty_low_min = int(10000*fifty_low_min)
	except: pass
	try:
		fifty_high_max = int(10000*fifty_high_max)
	except: pass

	kwargs = financial_info_search_logic(price_min, price_max, 'price')
	kwargs.update(financial_info_search_logic(vol_min, vol_max, 'volume'))
	kwargs.update(financial_info_search_logic(marketcap_min, marketcap_max, 'market_cap'))
	kwargs.update(financial_info_search_logic(shouts_min, shouts_max, 'outstanding'))
	kwargs.update(financial_info_search_logic(fifty_low_min, fifty_high_max, 'fiftytwo_week_low', 'fiftytwo_week_high'))

	return sqs.filter(**kwargs)

def financial_info_search_logic(num_min, num_max, whichone, whichone2='BIGALOW'):
	if whichone2 == 'BIGALOW':
		whichone2 = whichone

	if not num_min and not num_max:
		return {}
	elif num_min and not num_max:
		return {(whichone + '__gte'):num_min}
	elif not num_min and num_max:
		return {(whichone2 + '__lte'):num_max}
	else:
		if whichone == whichone2:
			return {(whichone + '__range'):[num_min, num_max]}
		else:
			return {(whichone + '__gte'):num_min, (whichone2 + '__lte'):num_max}


def list_in_list_search(sqs, args):
	'''(sqs, *args)
	args = [(whichone, [data]), (whichone, [data])]'''
	for whichone, data in args:
		for i, item in enumerate(data):
			try:
				sq = sq | SQ(**{whichone+'__exact':item})
			except:
				sq = SQ(**{whichone+'__exact':item})

	# sqs = sqs.filter(sq)
	# raise Exception
	try: return sqs.filter(sq)
	except: return sqs


def list_in_list_search_AND(sqs, args):
	'''(sqs, *args)
	args = [(whichone, [data]), (whichone, [data])]'''
	for whichone, data in args:
		for i, item in enumerate(data):
			try:
				sq = sq & SQ(**{whichone+'__exact':item})
			except:
				sq = SQ(**{whichone+'__exact':item})

	# sqs = sqs.filter(sq)
	# raise Exception
	try: return sqs.filter(sq)
	except: return sqs


@csrf_protect
def search(request, extra_context={}):
	ads_dict = {'results_broker_side_ad':single_ad_picker(6), 'results_client_side_ad':single_ad_picker(4)}


	args = {"title": "Search results"}
	args.update(extra_context)
	args.update(ads_dict)

	if request.user.is_authenticated():
		fin_form = FinancialInfoSearchForm(request.GET)
	else:
		fin_form = FinancialInfoDisabledForm()

	prop_form = PropertySearchForm(request.GET)
	forms_dict = {'mainbox_fin_search': fin_form,\
					'mainbox_prop_search': prop_form}
	

	# this Q should only be ajax, or at most work with only companies
	q = request.GET.get('q')
	if q:
		args.update({'navbar_search': NavBarSearch(request.GET)})
		companies = SearchQuerySet().models(Company).filter(SQ(content=('*'+q+'*')) | SQ(content__startswith=q))
		args.update({"results": companies})
		args.update(select2_dict)
		args.update(forms_dict)
		return render_to_response('search/results.html', args, context_instance=RequestContext(request))

	# re-showing the fields 
	location_list = request.GET.getlist('location')
	try:
		selected_regions = [x.split('_')[1] for x in location_list if x.startswith('region_')]
	except:
		pass
	try:
		selected_countries = [x.split('_')[1] for x in location_list if x.startswith('country_')]
	except:
		pass
	try:
		selected_states = [x.split('_')[1] for x in location_list if x.startswith('state_')]
	except:
		pass
	try:
		selected_commodities = [x for x in request.GET.getlist('commodities')]
	except:
		pass

	selected_status = request.GET.getlist('status')

	selected_estimate = request.GET.get('estimate')
	args.update({'selected_regions': selected_regions, 'selected_countries':selected_countries, 'selected_states':selected_states,\
				'selected_status':selected_status, 'selected_estimate':selected_estimate, 'YNtuple': ['Y', 'N'], 'selected_commodities': selected_commodities})
	# end re-shwoing

	# start search logic
	sqs = SearchQuerySet()

	if prop_form.is_bound and prop_form.is_valid() and selected_status and 'A' not in selected_status:
		pass
		# status
		sqs = list_in_list_search(sqs, [('status', selected_status)])


	# commodity
	sqs = list_in_list_search_AND(sqs, [('commodities', request.GET.getlist('commodities'))])

	# estimate
	if selected_estimate and 'A' not in selected_estimate:
		sqs = sqs.filter(SQ(estimate=selected_estimate) | SQ(estimate='A'))

	#location
	sqs = list_in_list_search(sqs, [('regions', selected_regions), ('countries', selected_countries), ('provinces', selected_states)])

	# Financial Info
	if fin_form.is_bound and fin_form.is_valid():
		sqs = financial_info_search(sqs, fin_form)
	# end search logic

	# raise Exception
	args.update(select2_dict)
	args.update(forms_dict)
	args.update({"results": sqs.models(Company)})
	return render_to_response('search/results.html', args, context_instance=RequestContext(request))


def navbar_ajax_search(request):
	pass