from django.db import models
import datetime
from django.utils import timezone
from decimal import Decimal


# def status_coder(s):
# 	return_str = ""
# 	if "D" in s:
# 		return_str += "D"
# 	else:
# 		return_str += "_"
# 	if "E" in s:
# 		return_str += "E"
# 	else:
# 		return_str += "_"
# 	if "P" in s:
# 		return_str += "P"
# 	else:
# 		return_str += "_"
# 	return unicode(return_str)


class Commodity(models.Model):
	'''The Commodity class. Stores a commodity.
	Has many-to-many relationships with Property.'''

	commodity_name = models.CharField(max_length=50, unique=True)

	commodity_price = models.DecimalField(max_digits=11, decimal_places=4, null=True, blank=True)
	commodity_price_change = models.DecimalField(max_digits=11, decimal_places=4, null=True, blank=True)

	feed_ticker_symbol = models.CharField(max_length=20, null=True, blank=True)

	def percent_change(self):
		try:
			return '%0.2f'%(100*(self.commodity_price_change/self.commodity_price))
		except:
			return 0

	class Meta:
		verbose_name_plural = 'Commodities'
		ordering = ['commodity_name']

	def __unicode__(self):
		return unicode(self.commodity_name)


class StockMarket(models.Model):
	"""A stock market with some companies associated with it"""
	market_name = models.CharField(max_length=50)
	short_name = models.CharField(max_length=20, unique=True)
	stock_index = models.DecimalField('Stock market index', max_digits=11, decimal_places=4, null=True, blank=True)
	stock_index_change = models.DecimalField('Change in stock market index', max_digits=11, decimal_places=4, null=True, blank=True)

	feed_ticker_symbol = models.CharField(max_length=20, null=True, blank=True)

	feed_grab_symbol = models.CharField(max_length=20, null=True, blank=True)
	chart_grab_symbol = models.CharField(max_length=20, null=True, blank=True)

	def percent_change(self):
		try:
			return '%0.2f'%(100*(self.stock_index_change/self.stock_index))
		except:
			return 0


	def __unicode__(self):
		return unicode(self.market_name)


class Company(models.Model):
	"""
	A class containing all the info of a public company in stock exchange markets.
	These info include a set of all the financial info, the company name and description,
	a set of all the properties and officers, contact info, rss news link and their social
	media links. It also keeps track of the Sponsership tier of this company and the users
	who follow/track it. Additionally it knows when it was last edited.
	"""

	# a company has a ticker symbol and a stock market, the combination of the two should be unique
	ticker_symbol = models.CharField('Ticker Symbol', max_length=10)
	stock_market = models.ForeignKey(StockMarket)
	# a set of functions that return the ticker symbol for feed or display purposes
	def ticker_display(self):
		return u"%s: %s" %(self.stock_market.short_name, self.ticker_symbol)

	def feed_grab_symbol(self):
		return u"%s%s" %(self.ticker_symbol, self.stock_market.feed_grab_symbol)

	def chart_grab_symbol(self):
		return u"%s%s" %(self.ticker_symbol, self.stock_market.chart_grab_symbol)


	# the logo of the comapny stored as a jpg file.
	logo = models.ImageField(upload_to='logos', blank=True)
	company_name = models.CharField('Company Name', max_length=50)
	company_desc = models.TextField('Company Description', blank=True)
	comp_add_info = models.TextField('Additional Info', blank=True) # clients only
	rss_news = models.URLField('Rss News Link', blank=True)

	email_capture_link = models.URLField('Subscribe Link', blank=True)

	COMP_LVL_CHOICES = (
		(0, 'Non-Client'),
		(1, 'Client'),
		(2, 'Premium Client'),
	)
	company_level = models.SmallIntegerField('Sponsership Tier', choices = COMP_LVL_CHOICES,
		default = 0)

	looking_for_commodities = models.ManyToManyField(Commodity, null=True, blank=True)

	edit_date = models.DateTimeField('Last Edit Date', auto_now=True)

	# DB relationships has been defined for these variables:
	# The set of financial info: 1-1 relation
	# The list of financial history: one to many (ForeignKey) relation
	# The set of contact info: 1-1 relation
	# The set of social media links: 1-1 relation
	# The list of properties: one to many (ForeignKey) relation
	# The list of officers: one to many (ForeignKey) relation
	# The list of followers: many to many relationship with the User object

	def was_updated_recently(self):
		now = timezone.now()
		return now - datetime.timedelta(days=30) <= self.pub_date <  now
	was_updated_recently.boolean = True
	was_updated_recently.short_description = 'Updated Recently?'

	# Search preperations
	def prop_commodities(self):
		'''Return a set of all the commodities of this company'''
		s = set(self.looking_for_commodities.all())
		for prop in self.property_set.all():
			s.update(list(prop.commodities.all()))
		return {unicode(item) for item in s}
	prop_commodities.short_description = 'Commodities of this company'

	def prop_regions(self):
		'''Return a set of all the regions that this company has properties in'''
		s = set()
		for prop in self.property_set.all():
			s.add(unicode(prop.state.country.region.region_name))
		return s
	prop_regions.short_description = 'Regions of this company'

	def prop_countries(self):
		'''Return a set of all the countries that this company has properties in'''
		s = set()
		for prop in self.property_set.all():
			s.add(unicode(prop.state.country.country_name))
		return s
	prop_countries.short_description = 'Countries of this company'

	def prop_provinces(self):
		'''Return a set of all the provinces that this company has properties in'''
		s = set()
		for prop in self.property_set.all():
			s.add(unicode(prop.state.state_name))
		return s
	prop_provinces.short_description = 'provinces of this company'

	def prop_status(self):
		s = set()
		prop_set = self.property_set.all()
		for prop in prop_set:
			s.add(prop.status)
		if not prop_set:
			s.add('E')
		return s
	prop_status.short_description = 'Status of this company'

	def prop_estimate(self):
		'''Return A if this company has properties with both estimates, Y if all properties are true, N otherwise'''
		s = set()
		for prop in self.property_set.all():
			s.add(prop.estimate)
		if True in s and False in s:
			return u'A'
		elif True in s:
			return u'Y'
		elif False in s:
			return u'N'
		else:
			return u'A'
	prop_estimate.short_description = 'Estimate of this company'

	class Meta:
		verbose_name = 'Company'
		verbose_name_plural = 'Companies'
		unique_together = ('ticker_symbol', 'stock_market',)
		ordering = ['company_name']

	def __unicode__(self):
		return unicode(self.ticker_display())


# -------------------------------------------------------------------------------------

class ContactInfo(models.Model):
	'''The ContactInfo class. Stores the contact info of a company.'''

	# the company whom this contact info belongs to
	company = models.OneToOneField(Company)

	# the set of all the info
	address = models.CharField('Address', max_length=255, blank=True)
	tel = models.CharField('Telephone', max_length=22, blank=True)
	toll_free = models.CharField('Toll Free Number', max_length = 28, blank=True)
	fax = models.CharField('Fax', max_length=22, blank=True)
	email = models.EmailField('Email', blank=True)
	website = models.URLField('Website', blank=True)

	class Meta:
		verbose_name_plural = 'Contact info'

	def __unicode__(self):
		return u"%s's Contact Info" % self.company


def shorter_int(x):
	try: int(x)
	except: return '0'
	if x < 1000: return str(x)
	elif x < 1000000: return '%0.2fK'%(x/1000.0)
	elif x < 1000000000: return '%0.2fM'%(x/1000000.0)
	else:  return '%0.2fB'%(x/1000000000.0)

class FinancialInfo(models.Model):
	''' The FinancialInfo class. Stores the financial information.
	Can update itself with latest data available online with a separate script.
	'''

	# the company whom this financial info belongs to
	company = models.OneToOneField(Company)

	price = models.DecimalField('Last Trade Value', max_digits=11, decimal_places=4, null=True, blank=True)

	change_in_price = models.DecimalField('Change in price', max_digits=11, decimal_places=4, null=True, blank=True)

	volume = models.IntegerField('Volume', null=True, blank=True)
	avg_daily_vol = models.IntegerField('Average Daily Volume', null=True, blank=True)
	fifty_day_average = models.DecimalField('50 Day Average', max_digits=10, decimal_places=3, null=True, blank=True)
	twohundred_day_average = models.DecimalField('200 Day Average', max_digits=10, decimal_places=3, null=True, blank=True)
	fiftytwo_week_low = models.DecimalField('52 Week Low', max_digits=10, decimal_places=3, null=True, blank=True)
	fiftytwo_week_high = models.DecimalField('52 Week High', max_digits=10, decimal_places=3, null=True, blank=True)
	market_cap = models.IntegerField('Market Cap', null=True, blank=True)
	outstanding = models.BigIntegerField('Shares Outstanding', null=True, blank=True)
	# It should be displayed as an option (like checkbox maybe?)
	override_outstanding = models.BooleanField('Override Shares Outstanding', default = False)

	def shorter_volume(self):
		return shorter_int(self.volume)

	def shorter_mkcap(self):
		return shorter_int(self.market_cap)

	def shorter_outstanding(self):
		return shorter_int(self.outstanding)

	class Meta:
		verbose_name_plural = 'Financial Info'

	def __unicode__(self):
		return u"%s's most recent Financial Info" % self.company


class FinancialHistory(models.Model):
	''' Stores the history of financial info of a company
	'''

	# the company whom this financial history belongs to
	company = models.ForeignKey(Company)

	price = models.DecimalField('Last Trade Value', max_digits=11, decimal_places=4, null=True, blank=True)

	change_price = models.DecimalField('Change in price', max_digits=11, decimal_places=4, null=True, blank=True)

	volume = models.IntegerField('Volume', null=True, blank=True)
	avg_daily_vol = models.IntegerField('Average Daily Volume', null=True, blank=True)
	fifty_day_average = models.DecimalField('50 Day Average', max_digits=10, decimal_places=3, null=True, blank=True)
	twohundred_day_average = models.DecimalField('200 Day Average', max_digits=10, decimal_places=3, null=True, blank=True)
	fiftytwo_week_low = models.DecimalField('52 Week Low', max_digits=10, decimal_places=3, null=True, blank=True)
	fiftytwo_week_high = models.DecimalField('52 Week High', max_digits=10, decimal_places=3, null=True, blank=True)
	market_cap = models.IntegerField('Market Cap', null=True, blank=True)
	outstanding = models.BigIntegerField('Shares Outstanding', null=True, blank=True)

	date = models.DateField(auto_now=True)

	class Meta:
		verbose_name = 'Financial History'
		verbose_name_plural = 'History of Financial Info'

	def __unicode__(self):
		return u"Financial info of company %s at date %s" % (self.company, self.date)



class OfficerPosition(models.Model):
	'''The OfficerPosition class. Stores info about a particular position. Has
	many-to-many relationships with Officer.'''

	position_name = models.CharField(max_length=50, unique=True)

	def __unicode__(self):
		return unicode(self.position_name)

class Officer(models.Model):
	'''The Officer class. Stores info about a particular officer.
	Has many-to-many relationships with OfficerPosition.'''

	company = models.ForeignKey(Company)

	officer_name = models.CharField(max_length=100)

	positions = models.ManyToManyField(OfficerPosition)

	# The list of positions is defined with a Many-to-many relationship with
	#the OfficerPosition object

	def __unicode__(self):
		return "%s's officer: %s" %(self.company, self.officer_name)


class Region(models.Model):
	"""
	A class storing a particular region.
	"""

	region_name = models.CharField(max_length=50, unique=True)
	class Meta:
		ordering = ['region_name']

	def __unicode__(self):
		return unicode(self.region_name)

class Country(models.Model):
	"""
	A class storing a particular country.
	"""
	region = models.ForeignKey(Region)
	country_name = models.CharField(max_length=50)

	class Meta:
		verbose_name_plural = 'Countries'
		unique_together = ('country_name', 'region')
		ordering = ['country_name']

	def __unicode__(self):
		return u'%s, %s'%(self.region, self.country_name)

class State(models.Model):
	"""
	A class storing a particular state.
	"""
	state_name = models.CharField(max_length=50, blank=True)
	country = models.ForeignKey(Country)

	class Meta:
		verbose_name = 'State/Province'
		verbose_name_plural = 'States/Provinces'
		unique_together = ('state_name', 'country')
		ordering = ['state_name']

	def __unicode__(self):
		if self.state_name == '':
			return u'%s, N/A'%self.country
		return u'%s, %s'%(self.country, self.state_name)


class Property(models.Model):
	'''The Property class. Stores info about a particular property of a company.
	Has many-to-many relationships with Commodity.'''

	property_name = models.CharField(max_length=100)

	company = models.ForeignKey(Company)

	# The list of commodities is defined with a Many-to-many relationship with the
	# commodity object
	commodities = models.ManyToManyField(Commodity)

	state = models.ForeignKey(State, verbose_name=u'Location')

	#An enumerated Tuple used for storing the status variable
	DEV = 'D'
	EXP = 'E'
	PRO = 'P'
	STATUS_CHOICES = (
		(DEV, 'Development'),
		(EXP, 'Exploration'),
		(PRO, 'Production'),
	)
	status = models.CharField(max_length=1, choices = STATUS_CHOICES,
		default = EXP)
	ownership = models.DecimalField(max_digits=5, decimal_places=2)
	option = models.DecimalField(max_digits=5, decimal_places=2)
	estimate = models.BooleanField('Resource estimate')

	edit_date = models.DateTimeField('Last Edit Date', blank=True, null=True, )

	def __unicode__(self):
		return "%s's property: %s" %(self.company, self.property_name)

	class Meta:
		verbose_name_plural = 'Properties'


class SocialMedia(models.Model):
	"""
	A class containing the social networking info of a particular company.
	"""

	company = models.OneToOneField(Company)
	facebook = models.URLField('Facebook', blank=True)
	twitter = models.URLField('Twitter', blank=True)
	google = models.URLField('43-101', blank=True)
	flickr = models.URLField('Flickr', blank=True)
	pinterest = models.URLField('Presentation', blank=True)
	tumblr = models.URLField('YouTube', blank=True)
	instagram = models.URLField('Fact Sheet', blank=True)

	def __unicode__(self):
		return u"%s's Social Media Info"%self.company

	class Meta:
		verbose_name_plural = 'Social Media'


# force creating financial info with companies
def create_model_b(sender, instance, created, **kwargs):
	"""Create FinancialInfo for every new Company."""
	if created:
		if not FinancialInfo.objects.filter(company=instance):
			FinancialInfo.objects.create(company=instance)

		# if not ContactInfo.objects.filter(company=instance):
		# 	ContactInfo.objects.create(company=instance)

		# if not SocialMedia.objects.filter(company=instance):
		# 	SocialMedia.objects.create(company=instance)

models.signals.post_save.connect(create_model_b, sender=Company, weak=False,
						  dispatch_uid='models.create_model_b')
