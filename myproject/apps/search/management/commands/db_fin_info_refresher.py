from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from apps.search.models import Company, FinancialInfo, StockMarket, Commodity

from decimal import Decimal, InvalidOperation
import urllib
import csv


def decimal_convertor(s):
	try:
		return Decimal(s)
	except InvalidOperation: return 0

def int_convertor(s):
	try:
		return int(s)
	except: return 0


def get_comma_seperated_ticker_list(list_of_ticker_symbols, stock_market=None):
	'''return a list of comma seperated ticker symbols. 200 tickers in each string element of the list'''

	l = []
	comma_seperated_tick_sym_str = ''
	i = 0
	for symbol in list_of_ticker_symbols:
		if symbol:
			# after every 200 symbols we need a new string of ticker symbols
			if i >= 200:
				l.append(comma_seperated_tick_sym_str)
				comma_seperated_tick_sym_str = ''
				i = 0
			try:
				comma_seperated_tick_sym_str += symbol+ stock_market.feed_grab_symbol +','
			except:
				comma_seperated_tick_sym_str += symbol+','
			i += 1

	l.append(comma_seperated_tick_sym_str)
	return l


def get_ticker_list_from_comps():
	'''From the list of all comapnies in Company model, return a list off all ticker symbols'''

	# prone to bugs
	# return Company.objects.values_list('ticker_symbol', flat=True).order_by('ticker_symbol')
	return FinancialInfo.objects.values_list('company__ticker_symbol', flat=True).order_by('company__ticker_symbol')


def get_list_of_comp_objects_fin_info_orderby_tick_sym():
	'''Return a list of FinancialInfo objects sorted by their respective company's ticker_symbol'''

	return FinancialInfo.objects.order_by('company__ticker_symbol')


def get_list_of_stock_markets_orderby_tick_sym():
	'''Return a list of StockMarket objects sorted by their respective company's ticker_symbol'''

	return StockMarket.objects.order_by('feed_ticker_symbol')


def get_list_of_commos_orderby_tick_sym():
	'''Return a list of Commodity objects sorted by their respective company's ticker_symbol'''

	return Commodity.objects.order_by('feed_ticker_symbol')



def url_request(symbols, frmt):
	'''Given a symbol or comma_seperated_tick_sym return a csv file of the yahoo feed.'''

	url = 'http://finance.yahoo.com/d/quotes.csv?s=%s&f=%s' % (symbols, frmt)
	return csv.reader(urllib.urlopen(url))


def int_withK(s):
	'''Market Caps in yahoo feed have this format: 23K or 45M. Convert them to int'''

	if s == 'N/A': return None
	elif s[-1].isdigit(): return Decimal(s)
	elif s[-1] == 'K': return Decimal(s[:-1]) * 1000
	elif s[-1] == 'M': return Decimal(s[:-1]) * 1000000
	elif s[-1] == 'B': return Decimal(s[:-1]) * 1000000000
	else: return None

def build_complete_csv_array(symbols_list, stock_market=None):
	'''From the list of symbols, build a 2d array of financial info feed from yahoo'''


	cs_sym_list = get_comma_seperated_ticker_list(symbols_list, stock_market)
	array = []

	for cs_str in cs_sym_list:
		# format
		# Symbol		Last	Volume	52-weekLow	52-weekHigh	AvgVol	MktCap	50dMA	200dMA   change in price
		#    0  	 	1 		2 		    3 			  4 	  5 	  6 	 7 		   8 			9
		# s            l1       v      		j			  k 	  a2 	 j1	     m3		   m4			c1
		array.extend(list(url_request(cs_str, 'sl1vjka2j1m3m4c1')))

	return array


def fin_info_writer_one(fin_info_object, row):
	'''from one row of fin info array, update one FinancialInfo object'''

	# check if ticker symbols match. If they don't, something in the orderby has gone wrong
	if fin_info_object.company.ticker_symbol+'.V' == row[0]:
		fin_info_object.price = decimal_convertor(row[1])
		fin_info_object.volume = int_convertor(row[2])
		fin_info_object.fiftytwo_week_low = decimal_convertor(row[3])
		fin_info_object.fiftytwo_week_high = decimal_convertor(row[4])
		fin_info_object.avg_daily_vol = int_convertor(row[5])
		fin_info_object.fifty_day_average = decimal_convertor(row[7])
		fin_info_object.twohundred_day_average = decimal_convertor(row[8])
		fin_info_object.change_in_price = decimal_convertor(row[9])

		# check for existance of market cap:
		mk_cap = int_withK(row[6])
		if not fin_info_object.override_outstanding:
			if mk_cap:
				fin_info_object.market_cap = mk_cap
				fin_info_object.outstanding = mk_cap / fin_info_object.price
			else: 
				fin_info_object.override_outstanding = True
		# Everything is updated, save the object to put it in the database
		fin_info_object.save()
	else:
		raise Exception('Order got wrong at %s and %s'%(fin_info_object, row[0]))
	

def get_ticker_list_from_stocks():
	'''From the list of all stock markets in stock market model, return a list off all ticker symbols'''

	# prone to bugs
	# return Company.objects.values_list('ticker_symbol', flat=True).order_by('ticker_symbol')
	return StockMarket.objects.values_list('feed_ticker_symbol', flat=True).order_by('pk')


def get_ticker_list_from_commos():
	'''From the list of all commodities in commodities model, return a list off all ticker symbols'''

	# prone to bugs
	# return Company.objects.values_list('ticker_symbol', flat=True).order_by('ticker_symbol')
	return Commodity.objects.values_list('feed_ticker_symbol', flat=True).order_by('pk')


def fin_info_writer_all():
	'''The main function. Gets a list of companies' financial info and using their ticker symbols pulls
	data from yahoo. Then updates the objects with the new data'''
	comp_symbols_list = get_ticker_list_from_comps()
	stock_symbols_list = get_ticker_list_from_stocks()
	commo_symbols_list = get_ticker_list_from_commos()

	fin_info_objects_list = get_list_of_comp_objects_fin_info_orderby_tick_sym()
	fin_info_array = build_complete_csv_array(comp_symbols_list, StockMarket.objects.get(short_name="TSX-V"))

	for i in range(len(fin_info_array)):
		row = fin_info_array[i]
		try:
			fin_info_object = fin_info_objects_list[i]
		except IndexError:
			fin_info_object = FinancialInfo(company=Company.objects.get(ticker_symbol=row[0]))
			fin_info_object.save()
		fin_info_writer_one(fin_info_object, row)

	stock_info_array = build_complete_csv_array(stock_symbols_list)
	print stock_info_array
	for i, stock_ticker_array in enumerate(stock_info_array):
		stock_ticker = stock_ticker_array[0]
		if stock_ticker:
			stk = StockMarket.objects.get(feed_ticker_symbol = stock_ticker)
			print stock_ticker
			try:
				stk.stock_index = decimal_convertor(stock_ticker_array[1])
				print stk.stock_index
				stk.stock_index_change = decimal_convertor(stock_ticker_array[9])
				print stk.stock_index_change
			except IndexError: print stk
			stk.save()

	commo_info_array = build_complete_csv_array(commo_symbols_list)
	i = 0
	for commo_ticker in commo_symbols_list:
		if commo_ticker:	
			commo = Commodity.objects.get(feed_ticker_symbol = commo_ticker)
			try:
				commo.commodity_price = decimal_convertor(commo_info_array[i][1])
				commo.commodity_price_change = decimal_convertor(commo_info_array[i][9])
			except IndexError: print commo
			commo.save()
			i += 1


class Command(NoArgsCommand):
	def handle_noargs(self, **options):
		fin_info_writer_all()





