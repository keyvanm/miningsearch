from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from apps.search.models import Company, FinancialInfo, FinancialHistory
from django.utils import timezone


def one_company_updator(comp):
	fin = comp.financialinfo
	comp.financialhistory_set.create(price=fin.price, volume=fin.volume, avg_daily_vol=fin.avg_daily_vol,\
	 	fifty_day_average=fin.fifty_day_average, twohundred_day_average=fin.twohundred_day_average,\
	  	fiftytwo_week_low=fin.fiftytwo_week_low, fiftytwo_week_high=fin.fiftytwo_week_high,\
	  	market_cap=fin.market_cap, outstanding=fin.outstanding)


class Command(NoArgsCommand):
	def handle_noargs(self, **options):
		all_companies = Company.objects.order_by('ticker_symbol')

		for comp in all_companies:
			one_company_updator(comp)