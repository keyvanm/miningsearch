from django.core.management.base import BaseCommand, CommandError, NoArgsCommand

from apps.search import models as oldmodels
from apps.search_new import models as newmodels

from decimal import Decimal


def onehundred(x):
	if type(x) == int:
		if x > 100:
			return 100
		else:
			return x
	else:
	 	return 0


def none2empty(s):
	try:
		if s.strip().lower() == 'none':
			return ''
		else:
			return s
	except: 
		return ''


class Command(NoArgsCommand):
	def handle_noargs(self, **options):
		
		# for oldcommo in oldmodels.Commodity.objects.all():
		# 	newcommo = newmodels.Commodity(commodity_name=oldcommo.name)
		# 	newcommo.save()


		# for old_region in oldmodels.Region.objects.all():

		# 	new_region = newmodels.Region(region_name=old_region.region_name)
		# 	new_region.save()
		# 	for old_country in old_region.country_set.all():
		# 		try:
		# 			new_country = newmodels.Country(region=new_region, country_name=old_country.country_name)
		# 			new_country.save()
		# 			for old_state in old_country.state_set.all():
		# 				try:
		# 					if old_state.state_name == 'Any':
		# 						new_state = newmodels.State(country=new_country, state_name='')
		# 					else:
		# 						new_state = newmodels.State(country=new_country, state_name=old_state.state_name)
		# 					new_state.save()
		# 				except: pass
		# 		except: pass


		# for oldpos in oldmodels.OfficerPosition.objects.all():
		# 	newpos = newmodels.OfficerPosition(position_name=oldpos.position)
		# 	newpos.save()


		stock_market = newmodels.StockMarket.objects.all()[0]

		for old_company in oldmodels.Company.objects.all():

			# COMPANY
			RSS = none2empty(old_company.rss_news)
				
			new_company = newmodels.Company(ticker_symbol=old_company.ticker_symbol.split('.')[0],\
											stock_market=stock_market,\
											logo=old_company.logo,\
											company_name = old_company.company_name.title(),\
											company_desc=old_company.company_desc,\
											comp_add_info=old_company.comp_add_info,\
											rss_news=RSS,\
											company_level=old_company.company_level)
			new_company.save()

			# CONTACT INFO
			try:
				old_contactinfo = old_company.contactinfo

				new_contactinfo = newmodels.ContactInfo(company=new_company,\
														address=none2empty(old_contactinfo.address),\
														tel=none2empty(old_contactinfo.tel),\
														toll_free=none2empty(old_contactinfo.toll_free),\
														fax=none2empty(old_contactinfo.fax),\
														email=none2empty(old_contactinfo.email),\
														website=none2empty(old_contactinfo.website))
				new_contactinfo.save()
			except:
				new_contactinfo = newmodels.ContactInfo(company=new_company)
				new_contactinfo.save()



			#OFFICERS
			for old_officer in old_company.officer_set.all():
				new_officer = newmodels.Officer(company=new_company, \
												officer_name = old_officer.name, \
												)
				new_officer.save()

				for old_positions in old_officer.position.all():
					new_officer.positions.add(newmodels.OfficerPosition.objects.get(position_name=old_positions.position))

			#PROPERtY
			for old_property in old_company.property_set.all():

				old_state = old_property.state
				old_state_name = ""
				if old_state.state_name.lower() != "any":
					old_state_name =  old_state.state_name

				old_country_name = old_state.country.country_name

				if not old_state_name:
					new_state = newmodels.State.objects.filter(country = newmodels.Country.objects.filter(country_name = old_country_name)[0])[0]
				else:
					try:
						new_state = newmodels.State.objects.filter(state_name = old_state_name, country = newmodels.Country.objects.get(country_name = old_country_name))[0]
					except:
						print 'state problem', new_company

				try:
					new_property = newmodels.Property(company=new_company, \
													property_name = old_property.name, \
													state = new_state, \
													status = old_property.status, \
													ownership = Decimal(onehundred(old_property.ownership)), \
													option = Decimal(onehundred(old_property.option)), \
													estimate = old_property.estimate, \
											)
					new_property.save()
				except:
					print 'prop problem', new_company, old_property

				for old_commodity in old_property.commodity.all():
					new_property.commodities.add(newmodels.Commodity.objects.get(commodity_name=old_commodity.name))


			# social info
			newmodels.SocialMedia(company=new_company).save()

