import MySQLdb
from django.utils import timezone
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from apps.search.models import *


def int_or_zero(x):
	try:
		return int(x)
	except: return 0

def bool_yes_no(x):
	try:
		if x.lower() == 'yes': return True
		else: return False
	except: return False


def property_list_maker(searchRow, staticRow):
	'''
	from the specified row in the dbs make a list of dicts of all the properties for the comp
	'''
	lst = []

	try:
		prop_names = searchRow[7].split(';')
		comms_list = searchRow[3].split(';')
		regions = searchRow[4].split(';')
		countries = searchRow[5].split(';')
		states = searchRow[6].split(';')
		statuss = searchRow[9].split(';')
		resEsts = searchRow[10].split(';')
		ownerships = staticRow[2].split(';')
		optionPercs = staticRow[2].split(';')

		for i in range(len(prop_names)):
			d = {}
			d['name'] = prop_names[i]
			d['comms'] = comms_list[i].split(',')
			d['region'] = regions[i]
			d['country'] = countries[i]
			d['state'] = states[i]
			d['status'] = statuss[i]
			d['resEst'] = bool_yes_no(resEsts[i])
			d['ownership'] = int_or_zero(ownerships[i])
			d['optionPerc'] = int_or_zero(optionPercs[i])
			lst.append(d)
	except Exception as e: print 'property_list_maker error: %s'%e
	return lst


def one_property_object_maker(company, d):
	'''
	from the dictionary make a property property_object
	'''
	try:
		try:
			state = State.objects.get(state_name=d['state'])
		except:
			state = State.objects.filter(country__country_name=d['country'])[0]
		prop = company.property_set.create(name=d['name'], status=d['status'][0].upper(),\
		 ownership=d['ownership'], option=d['optionPerc'], estimate=d['resEst'], state=state)

		state.property_set.add(prop)

		for commo in d['comms']:
			if not commo: continue
			try:
				comm = Commodity.objects.get_or_create(name=commo)[0]
				prop.commodity.add(comm)
			except Exception as e: print 'adding commodity error: %s company:%s'%(e, company)
	except Exception as e: print 'one_property_object_maker error %s company:%s'%(e, company)


def property_object_list_maker(company, searchRow, staticRow):
	'''
	from the input argument make a list of property obejcts
	'''
	lst = property_list_maker(searchRow, staticRow)

	for d in lst:
		one_property_object_maker(company, d)


def officers_maker(company, searchRow, staticRow):
	try:
		officerNameList = staticRow[4].split(';')
		officerPositionList = staticRow[5].split(';')
		if len(officerPositionList) != len(officerNameList): return
		for i in range(len(officerNameList)):
			try:
				if not officerPositionList[i]: continue
				officer = company.officer_set.create(name=officerNameList[i])
				pos = OfficerPosition.objects.get_or_create(position=officerPositionList[i])[0]
				officer.position.add(pos)
			except Exception as e: print 'adding officer error: %s company:%s'%(e, company)
	except Exception as e: print 'officers_maker error %s company:%s'%(e, company)





# TODO make the fin info fields in models to be blank=True DONE

class Command(NoArgsCommand):
	def handle_noargs(self, **options):
		# open a mysql conncection
		if False: #remote connection
			conn = MySQLdb.connect (host = "69.161.223.240",
									user = "emam",
									passwd = "manemamam",
									db = "MSSearchDB")
		else:
			conn = MySQLdb.connect (host = "web352.webfaction.com",
									user = "rblcomm_db",
									passwd = "rodylazar1",
									db = "rblcomm_db")
		c = conn.cursor()

		c.execute('SELECT * FROM StaticSearchDB ORDER BY symbol')
		SearchDB = c.fetchall()
		c.execute('SELECT * FROM StaticStaticDB ORDER BY symbol')
		StaticDB = c.fetchall()

		for row_index in range(len(SearchDB))[0:700]:
			try:
				searchRow = SearchDB[row_index]
				staticRow = StaticDB[row_index]

				if not searchRow[1]: raise Exception('on row %s, company %s, error occured: Company name connot be empty.'%(row_index, searchRow[0]))

				comp = Company(ticker_symbol = searchRow[0], company_name = searchRow[1],\
				 company_desc = staticRow[21], rss_news = staticRow[12], edit_date = timezone.now())

				comp.save()

				fin_info = FinancialInfo(company = comp)
				fin_info.save()

				contact_info = ContactInfo(company = comp, address = staticRow[6], tel = staticRow[7],\
					toll_free = staticRow[8], fax = staticRow[9], email = staticRow[10], website = staticRow[11])
				contact_info.save()

				soc_media = SocialMedia(company = comp)
				soc_media.save()

				officers_maker(comp, searchRow, staticRow)

				property_object_list_maker(comp, searchRow, staticRow)

			except Exception as e:
				print 'on row %s, company %s, error occured: %s'%(row_index, searchRow[0], e)







