from django.utils import timezone
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from django.db.utils import IntegrityError
from apps.search.models import *

commList = ['silver', 'gold', 'cobalt', 'coal', 'copper', 'zinc', 'diamonds', 'iron', 'lithium', 'molybdenum', 'nickel', 'lead', 'palladium', 'PGM ', 'platinum', 'rare earth elements', 'uranium', 'aggregate', 'graphite', 'ruthenium', 'aluminum', 'gypsum', 'sulfur', 'Boron', 'heavy minerals', 'salt', 'barium', 'ilmenite', 'antimony', 'base_metals', 'indium', 'scandium', 'beryllium', 'iridium', 'silicon', 'bismuth', 'kaolin', 'tin', 'borates', 'limestone', 'tantalum', 'calcium', 'magnesium', 'talc', 'chromium', 'manganese', 'tellurium', 'fluorite', 'niobium', 'TiO2', 'gallium', 'oil', 'vanadium', 'garnet', 'phosphate', 'tungsten', 'gas', 'potash', 'zeolite', 'germanium', 'rhenium', 'zirconium', 'gemstones', 'rhodium'];

regionList = ['North America', 'South America', 'Central America', 'Africa', 'Asia', 'Europe', 'Oceania']

NAmericaList = ['USA', 'Canada', 'Mexico'];

CAmericaList = ['Belize', 'Costa Rica', 'El Salvador', 'Guatemala', 'Honduras', 'Nicaragua', 'Panama']

SAmericaList = ['Argentina', 'Bolivia', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Falkland Islands', 'French Guiana', 'Guyana', 'Paraguay', 'Peru', 'South Georgia', 'Suriname', 'Uruguay', 'Venezuela'];


AfricaList = ['Burundi', 'Comoros', 'Djibouti', 'Eritrea', 'Ethiopia', 'Kenya', 'Madagascar', 'Malawi', 'Mauritius', 'Mayotte', 'Mozambique', 'R\xc3\xa9union', 'Rwanda', 'Seychelles', 'Somalia', 'Tanzania', 'Uganda', 'Zambia', 'Zimbabwe', 'Angola', 'Cameroon', 'Central African Republic', 'Chad', 'Republic of the Congo', 'Democratic Republic of the Congo', 'Equatorial Guinea', 'Gabon', 'S\xc3\xa3o Tom\xc3\xa9 and Pr\xc3\xadncipe', 'Algeria', 'Canary Islands', 'Ceuta', 'Egypt', 'Libya', 'Madeira', 'Melilla', 'Morocco', 'South Sudan', 'Sudan', 'Tunisia', 'Western Sahara', 'Botswana', 'Lesotho', 'Namibia', 'South Africa', 'Swaziland', 'Benin', 'Burkina Faso', 'Cape Verde', "C\xc3\xb4te d'Ivoire", 'Gambia', 'Ghana', 'Guinea', 'Guinea-Bissau', 'Liberia', 'Mali', 'Mauritania', 'Niger', 'Nigeria', 'Saint Helena', 'Senegal', 'Sierra Leone', 'Togo'];


AsiaList = ['Abkhazia', 'Afghanistan', 'Akrotiri and Dhekelia', 'Armenia', 'Azerbaijan', 'Bahrain', 'Bangladesh', 'Bhutan', 'British Indian Ocean Territory', 'Brunei', 'Cambodia', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Georgia', 'Hong Kong', 'India', 'Indonesia', 'Iran', 'Iraq', 'Israel', 'Japan', 'Jordan', 'Kazakhstan', 'North Korea', 'South Korea', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Macau', 'Malaysia', 'Maldives', 'Mongolia', 'Myanmar', 'Nagorno-Karabakh', 'Nepal', 'Oman', 'Pakistan', 'Philippines', 'Qatar', 'Russia', 'Saudi Arabia', 'Singapore', 'Sri Lanka', 'South Ossetia', 'Syria', 'Taiwan', 'Tajikistan', 'Thailand', 'Timor-Leste', 'Turkmenistan', 'United Arab Emirates', 'Uzbekistan', 'Vietnam', 'Yemen'];


EuropeList = ['Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan', 'Belarus', 'Belgium', 'Bosnia and Herzegovina', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Georgia', 'Germany', 'Greece', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Kazakhstan', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Republic of Macedonia', 'Malta', 'Moldova', 'Monaco', 'Montenegro', 'Netherlands', 'Norway', 'Poland', 'Portugal', 'Romania', 'Russia', 'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'Switzerland', 'Turkey', 'Ukraine', 'United Kingdom', 'Vatican City'];


OceaniaList = ['American Samoa', 'Ashmore and Cartier Islands', 'Australia', 'Cook Islands', 'Coral Sea Islands', 'Easter Island', 'Fiji', 'French Polynesia', 'Guam', 'Hawaii', 'Indonesia', 'Kiribati', 'Marshall Islands', 'Federated States of Micronesia', 'Nauru', 'New Caledonia', 'New Zealand', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Palau', 'Papua New Guinea', 'Pitcairn Islands', 'Samoa', 'Solomon Islands', 'Timor-Leste', 'Tokelau', 'Tonga', 'Tuvalu', 'Vanuatu', 'Wallis and Futuna'];



stateUSAList = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];


stateCanadaList = ['Ontario', 'Quebec', 'Nova Scotia', 'New Brunswick', 'Manitoba', 'British Columbia', 'Prince Edward Island', 'Saskatchewan', 'Alberta', 'Newfoundland and Labrador', 'Northwest Territories', 'Yukon', 'Nunavut'];


stateMexicoList = ['Mexico State', 'Nuevo Le\xc3\xb3n', 'Jalisco', 'Chihuahua', 'Veracruz', 'Puebla', 'Baja California', 'Guanajuato', 'Coahuila', 'Tamaulipas', 'Sonora', 'Michoac\xc3\xa1n', 'San Luis Potos\xc3\xad', 'Sinaloa', 'Quer\xc3\xa9taro', 'Chiapas', 'Guerrero', 'Quintana Roo', 'Oaxaca', 'Yucat\xc3\xa1n', 'Morelos', 'Durango', 'Hidalgo', 'Aguascalientes', 'Tabasco', 'Campeche', 'Zacatecas', 'Baja California Sur', 'Nayarit', 'Colima', 'Tlaxcala'];



class Command(NoArgsCommand):
	def handle_noargs(self, **options):

		# making all the commodities
		
		for c in commList:
			try:
				#print c
				comm = Commodity(name = c)
				comm.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all the regions
		
		for c in regionList:
			try:
				reg = Region(region_name = c)
				reg.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		# making all north america countries
		for c in NAmericaList:
			try:
				reg = Region.objects.get(region_name="North America")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		# making all central america countries
		for c in CAmericaList:
			try:
				reg = Region.objects.get(region_name="Central America")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all south america countries
		for c in SAmericaList:
			try:
				reg = Region.objects.get(region_name="South America")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all south america countries
		for c in AfricaList:
			try:
				reg = Region.objects.get(region_name="Africa")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all south america countries
		for c in AsiaList:
			try:
				reg = Region.objects.get(region_name="Asia")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all south america countries
		for c in EuropeList:
			try:
				reg = Region.objects.get(region_name="Europe")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all south america countries
		for c in OceaniaList:
			try:
				reg = Region.objects.get(region_name="Oceania")
				count = reg.country_set.create(country_name=c)
				count.save()
				state = count.state_set.create(state_name="Any")
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all USA states
		for c in stateUSAList:
			try:
				count = Country.objects.get(country_name="USA")
				state = count.state_set.create(state_name=c)
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all Canada states
		for c in stateCanadaList:
			try:
				count = Country.objects.get(country_name="Canada")
				state = count.state_set.create(state_name=c)
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)

		#making all Mexico states
		for c in stateMexicoList:
			try:
				count = Country.objects.get(country_name="Mexico")
				state = count.state_set.create(state_name=c)
				state.save()
			except Exception as e: print 'error %s at %s'%(e, c)



