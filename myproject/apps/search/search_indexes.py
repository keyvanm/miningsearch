from haystack.indexes import *
from haystack import site
from apps.search.models import Company, Officer, Property


class CompanyIndex(SearchIndex):
	# navbar search support
	text = CharField(document=True, use_template=True)

	# financial info search
	price = IntegerField(default=-1)
	volume = IntegerField(model_attr="financialinfo__volume", default=-1)
	outstanding = IntegerField(model_attr="financialinfo__outstanding", default=-1)
	market_cap = IntegerField(model_attr="financialinfo__market_cap", default=-1)
	fiftytwo_week_low = IntegerField(default=-1)
	fiftytwo_week_high = IntegerField(default=-1)

	def prepare_price(self, obj):
		try: return int(obj.financialinfo.price * Decimal('10000'))
		except: return -1

	def prepare_fiftytwo_week_low(self, obj):
		try: return int(obj.financialinfo.fiftytwo_week_low * Decimal('10000'))
		except: return -1

	def prepare_fiftytwo_week_high(self, obj):
		try: return int(obj.financialinfo.fiftytwo_week_high * Decimal('10000'))
		except: return -1


	# Property
	commodities = MultiValueField()
	def prepare_commodities(self, obj):
		return " , ".join(obj.prop_commodities())

	regions = MultiValueField()
	def prepare_regions(self, obj):
		return " , ".join(obj.prop_regions())

	countries = MultiValueField()
	def prepare_countries(self, obj):
		return " , ".join(obj.prop_countries())

	provinces = MultiValueField()
	def prepare_provinces(self, obj):
		return " , ".join(obj.prop_provinces())

	status = MultiValueField()
	def prepare_status(self, obj):
		return " , ".join(obj.prop_status())


	estimate = CharField(model_attr="prop_estimate")

	rendered_auth = CharField(use_template=True, indexed=False)
	rendered_notauth = CharField(use_template=True, indexed=False)

	# # check whether its been recently updated or not (useless now, it has to update because of fin info)
	# def get_updated_field(self):
	# 	return "edit_date"


class PropertyIndex(SearchIndex):
	text = CharField(document=True, use_template=True)


class OfficerIndex(SearchIndex):
	text = CharField(document=True, use_template=True)


site.register(Company, CompanyIndex)
site.register(Property, PropertyIndex)
site.register(Officer, OfficerIndex)
