from django.contrib import admin
from django.contrib.auth.models import User

from models import *
import reversion


class FinancialInfoInline(admin.TabularInline):
	model = FinancialInfo
	readonly_fields = ['price', 'volume', 'avg_daily_vol', 'fifty_day_average', 'twohundred_day_average',\
	 'fiftytwo_week_low', 'fiftytwo_week_high', 'market_cap', 'outstanding', 'override_outstanding']
	# fields = ['price', 'volume', 'avg_daily_vol', 'fifty_day_average', 'twohundred_day_average',\
	#  'fiftytwo_week_low', 'fiftytwo_week_high', 'market_cap', 'outstanding', 'override_outstanding']

class PropertyInline(admin.TabularInline):
	model = Property
	#fk_name = 'company'
	#chaninging the class of selects
	# frm = self.get_form()
	# frm.fields['state'].widget.attrs = {'class': 'chzn-select'}
	# filter_horizontal = ('commodity',)
	extra = 1
	fields = ['property_name', 'commodities', 'status', 'ownership', 'option', 'estimate', 'state']

class OfficerInline(admin.TabularInline):
	model = Officer
	# filter_horizontal = ('position',)
	extra = 1
	# class Media:
	# 	css = {
	# 		"all": ("shorter.css",)
	# 	}


class ContactInfoInline(admin.TabularInline):
	model = ContactInfo

class SocialMediaInline(admin.TabularInline):
	model = SocialMedia
	fields = ['facebook', 'twitter', 'tumblr', 'flickr', 'pinterest', 'instagram', 'google']


class StockMarketAdmin(admin.ModelAdmin):
	readonly_fields = ('stock_index', 'stock_index_change', )
	list_display = ('market_name', 'short_name', 'stock_index', 'stock_index_change', )
	search_fields = ('market_name', 'short_name', )


class CompanyAdmin(reversion.VersionAdmin):

	ignore_duplicate_revisions = True

	list_display = ('ticker_symbol', 'company_name', 'edit_date', 'company_level')
	search_fields = ('ticker_symbol', 'company_name')
	list_filter = ('company_level', 'edit_date')

	fields = ['stock_market', 'ticker_symbol', 'company_name', 'company_desc', 'comp_add_info', 'logo', 'looking_for_commodities', 'company_level', 'rss_news', ]

	inlines = [ContactInfoInline, OfficerInline, PropertyInline,
	SocialMediaInline] #, FinancialInfoInline]

	def get_readonly_fields(self, request, obj=None):
		if obj: # editing an existing object
			return self.readonly_fields + ('ticker_symbol', 'edit_date')
		return self.readonly_fields

	class Media:
		css = {
			 'all': ('stylesheets/select2.css', 'admin/select2-admin.css', )
		}
		js = ('javascript/jquery-1.8.3.min.js', 'javascript/select2.js', 'admin/select2-admin.js')


class CommodityAdmin(admin.ModelAdmin):
	list_display = ('commodity_name', 'commodity_price', 'commodity_price_change',)
	search_fields = ('commodity_name', )
	readonly_fields = ('commodity_price', 'commodity_price_change')

	def get_readonly_fields(self, request, obj=None):
		if not request.user.is_superuser: # editing an existing object
			return self.readonly_fields + ('feed_ticker_symbol',)
		return self.readonly_fields


class OfficerPositionAdmin(admin.ModelAdmin):
	search_fields = ('position', )

class FinancialHistoryAdmin(admin.ModelAdmin):
	def has_add_permission(self, request):
		return False
	readonly_fields = ['price', 'volume', 'avg_daily_vol', 'fifty_day_average', 'twohundred_day_average',\
	 'fiftytwo_week_low', 'fiftytwo_week_high', 'market_cap', 'outstanding', 'company', 'date']


class PropertyAdmin(admin.ModelAdmin):
	search_fields = ('property_name', )

class OfficerAdmin(admin.ModelAdmin):
	search_fields = ('officer_name', )


class RegionAdmin(admin.ModelAdmin):
	search_fields = ('region_name', )

class CountryAdmin(admin.ModelAdmin):
	search_fields = ('country_name', )

class StateAdmin(admin.ModelAdmin):
	search_fields = ('state_name', )



#registering them views with admin
admin.site.register(StockMarket, StockMarketAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Commodity, CommodityAdmin)
admin.site.register(OfficerPosition, OfficerPositionAdmin)

admin.site.register(State, StateAdmin)

admin.site.register(Country, CountryAdmin)
admin.site.register(Region, RegionAdmin)
# admin.site.register(FinancialInfo)
# admin.site.register(FinancialHistory, FinancialHistoryAdmin)
