from django import forms
from models import Commodity, Property


class FinancialInfoSearchForm(forms.Form):
	wg_min = forms.TextInput(attrs={'placeholder':'min', 'class':'span6', 'pattern':'\d*(\.\d+)?',\
	 'data-validation-pattern-message':'Must be a number or decimal'})
	wg_max = forms.TextInput(attrs={'placeholder':'max', 'class':'span6', 'pattern':'\d*(\.\d+)?',\
	 'data-validation-pattern-message':'Must be a number or decimal'})

	wg_low = forms.TextInput(attrs={'placeholder':'low', 'class':'span6', 'pattern':'\d*(\.\d+)?',\
	 'data-validation-pattern-message':'Must be a number or decimal'})
	wg_high = forms.TextInput(attrs={'placeholder':'high', 'class':'span6', 'pattern':'\d*(\.\d+)?',\
	 'data-validation-pattern-message':'Must be a number or decimal'})


	price_min = forms.DecimalField(widget=wg_min, required=False)
	price_max = forms.DecimalField(widget=wg_max, required=False)

	vol_min = forms.DecimalField(widget=wg_min, required=False)
	vol_max = forms.DecimalField(widget=wg_max, required=False)

	marketcap_min = forms.DecimalField(widget=wg_min, required=False)
	marketcap_max = forms.DecimalField(widget=wg_max, required=False)

	shouts_min = forms.DecimalField(widget=wg_min, required=False)
	shouts_max = forms.DecimalField(widget=wg_max, required=False)

	fifty_low_min = forms.DecimalField(widget=wg_low, required=False)

	fifty_high_max = forms.DecimalField(widget=wg_high, required=False)


class FinancialInfoDisabledForm(forms.Form):
	wg_min = forms.TextInput(attrs={'placeholder':'min', 'class':'span6', 'disabled':'disabled'})
	wg_max = forms.TextInput(attrs={'placeholder':'max', 'class':'span6', 'disabled':'disabled'})

	wg_low = forms.TextInput(attrs={'placeholder':'low', 'class':'span6', 'disabled':'disabled'})
	wg_high = forms.TextInput(attrs={'placeholder':'high', 'class':'span6', 'disabled':'disabled'})

	price_min = forms.DecimalField(widget=wg_min, required=False)
	price_max = forms.DecimalField(widget=wg_max, required=False)

	vol_min = forms.DecimalField(widget=wg_min, required=False)
	vol_max = forms.DecimalField(widget=wg_max, required=False)

	marketcap_min = forms.DecimalField(widget=wg_min, required=False)
	marketcap_max = forms.DecimalField(widget=wg_max, required=False)

	shouts_min = forms.DecimalField(widget=wg_min, required=False)
	shouts_max = forms.DecimalField(widget=wg_max, required=False)

	fifty_low_min = forms.DecimalField(widget=wg_low, required=False)

	fifty_high_max = forms.DecimalField(widget=wg_high, required=False)


class PropertySearchForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(PropertySearchForm, self).__init__(*args, **kwargs)
		# self.fields['commodities'].required = False

		self.fields['status'].required = False
	class Meta:
		model = Property
		fields = ('status',)# 'commodities')
		# widgets = {'commodities': forms.widgets.SelectMultiple(attrs={'class': 'span12'})}

