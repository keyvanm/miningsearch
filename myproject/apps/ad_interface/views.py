from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from apps.ad_interface.models import *


def ad_click(request, ad_pk, extra_context={}):

	# get the company defined in the URL
	ad = get_object_or_404(Ad, pk=ad_pk)
	ad.num_clicks += 1
	ad.save()

	return HttpResponseRedirect(ad.link)