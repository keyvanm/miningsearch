from django.db import models
from apps.search.models import Commodity, Company

import random



def multi_ad_picker(primary_key, number):
	ads = rsample((TypeOfAd.objects.get(pk=primary_key)).ad_set.all(), number)
	for ad in ads:
		ad.num_impressions += 1
		ad.save()
	return ads


def single_ad_picker(primary_key):
	try:
		ad = random.choice(TypeOfAd.objects.get(pk=primary_key).ad_set.all())
		ad.num_impressions += 1
		ad.save()
		return ad
	except IndexError:
		return None


def rsample(l, num):
	for i in range(num, -1, -1):
		try:
			s = random.sample(l, i)
			return s
		except ValueError: pass
	return []



class TypeOfAd(models.Model):
	type_name = models.CharField(max_length=25)

	optimal_height = models.IntegerField()
	optimal_width = models.IntegerField()

	def __unicode__(self):
		return unicode(self.type_name)
	


class Ad(models.Model):

	ad_name = models.CharField(max_length=25)

	picture = models.ImageField(upload_to='assetsd')

	type_of_ad = models.ManyToManyField(TypeOfAd)

	commodities = models.ManyToManyField(Commodity, blank=True, null=True)
	sponsor = models.ForeignKey(Company, blank=True, null=True)

	link = models.URLField(blank=True)


	num_impressions = models.IntegerField(default=0)
	num_clicks = models.IntegerField(default=0)

	hover_text = models.CharField(max_length=25, blank=True)

	class Meta:
		verbose_name_plural = 'Ads'

	def __unicode__(self):
		return unicode(self.ad_name)
