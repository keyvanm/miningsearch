from django.contrib import admin
from models import *



class TypeOfAdAdmin(admin.ModelAdmin):
	search_fields = ('type_name', )


class AdAdmin(admin.ModelAdmin):
	search_fields = ('ad_name', 'commodities', 'sponser')
	readonly_fields = ('num_impressions', 'num_clicks')
	# filter_horizontal = ('commodities', 'type_of_ad')

	class Media:
		css = {
			 'all': ('stylesheets/select2.css', 'admin/select2-admin.css', )
		}
		js = ('javascript/jquery-1.8.3.min.js', 'javascript/select2.js', 'admin/select2-admin.js')



admin.site.register(TypeOfAd, TypeOfAdAdmin)

admin.site.register(Ad, AdAdmin)