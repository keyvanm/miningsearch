from django.conf.urls import patterns, include, url


urlpatterns = patterns('apps.admintools.views',
	url(r'^suggestions/(?P<sg_pk>[^/]+)/approve/$', 'suggestions_approve'),
	url(r'^suggestions/(?P<sg_pk>[^/]+)/reject/$', 'suggestions_reject'),
	url(r'^suggestions/(?P<sg_pk>[^/]+)/$', 'suggestions_check'),
	url(r'^suggestions/$', 'suggestions'),
	url(r'^emailmanager/$', 'emailman_view'),
	url(r'^emailmanager/getemails/$', 'get_emails'),
	# url(r'^$', 'admintools_view'),
)