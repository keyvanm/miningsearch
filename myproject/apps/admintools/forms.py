from django import forms
import datetime

from apps.search.models import Company

# IS_FILTER_CHIOCES


class SinceDateForm(forms.Form):
	since = forms.DateField(initial=(datetime.date.today() - datetime.timedelta(7)), required=False)
	comps = forms.ModelMultipleChoiceField(required=False ,queryset=Company.objects.order_by('ticker_symbol'), widget=forms.SelectMultiple(attrs={'placeholder':'Select Companies',\
	 'class':'offset1 span6'}))
	# is_filter = forms.Radio