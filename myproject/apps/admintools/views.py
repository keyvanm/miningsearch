from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
# from reversion.helpers import generate_patch_html

from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext

from django.contrib.auth.models import User
from forms import *

from apps.company_profile_edit.models import VersionApprove
from apps.search.models import Company
import reversion

import datetime

import csv

from django.db.models import Q



def list_in_list_search(qs, args):
	'''(qs, *args)
	args = [(whichone, [data]), (whichone, [data])]'''
	for whichone, data in args:
		for i, item in enumerate(data):
			try:
				q = q | Q(**{whichone+'__exact':item})
			except:
				q = Q(**{whichone+'__exact':item})


	try: return qs.filter(q)
	except: return qs




@csrf_protect
@login_required
def emailman_view(request, extra_context={}):
	
	initialdate=(datetime.date.today() - datetime.timedelta(7))

	user = request.user

	if not user.is_authenticated() and not user.is_staff and not user.is_superuser:
		raise Http404



	args = {'title':"%s's admintools"%user.username, 'sinceform':SinceDateForm, 'initialdate': initialdate.isoformat()}
	args.update(extra_context)

	return render_to_response('admintools/emailman.html', args, context_instance=RequestContext(request))


@csrf_protect
@login_required
def get_emails(request, extra_context={}):
	is_filter = request.GET.get('is_filter')
	filter_date = request.GET.get('filter_date') == 'yes'
	filter_comps = request.GET.get('filter_comps') == 'yes'
	dont_include_comp_users = request.GET.get('include_comp_users') != 'yes'

	qs = User.objects.all()

	if is_filter != 'all':
		sdf = SinceDateForm(request.GET)
		if sdf.is_bound and sdf.is_valid():
			# filter by date
			if filter_date:
				date = sdf.cleaned_data.get('since')
				qs = qs.filter(date_joined__gte = date)
			# filter by liked company
			if filter_comps:
				#raise Exception
				comps = sdf.cleaned_data.get('comps')
				qs = list_in_list_search(qs, [('userprofile__tracked_companies', comps)])

	if dont_include_comp_users:
		qs = qs.filter(userprofile__is_company_user__isnull=True)



	response = HttpResponse(mimetype='text/csv')
	response['Content-Disposition'] = 'attachment; filename=emails.csv'

	writer = csv.writer(response)
	writer.writerow(['Username', 'Email', 'Date joined', 'Type'])


	for usr in qs:
		s = ''
		for tpe in usr.userprofile.type_of_investor.all():
			s+= '%s, '%tpe
		
		writer.writerow([usr.username ,usr.email, usr.date_joined.date().isoformat(), s])

	# raise Exception
	return response

@csrf_protect
@login_required
def suggestions(request, extra_context={}):

	user = request.user

	if not user.is_authenticated() or not user.is_staff:
		raise Http404



	args = {'submissions':reversion.models.Revision.objects.exclude(comment="Initial version.").exclude(user=None).order_by('-date_created'), 'title':'Suggestions'}
	args.update(extra_context)

	return render_to_response('admintools/suggestions.html', args, context_instance=RequestContext(request))


def comp_finder(versions):
	for i in versions:
		if isinstance(i.object, Company):
			return i
	return None


@csrf_protect
@login_required
def suggestions_check(request, sg_pk, extra_context={}):

	user = request.user

	if not user.is_authenticated() or not user.is_staff:
		raise Http404

	sg = get_object_or_404(reversion.models.Revision, pk=sg_pk)
	company = comp_finder(sg.version_set.all()).object
	latest_admin_approved = reversion.models.Version.objects.get_for_object(company).filter(revision__versionapprove__is_approved=1).order_by('-revision__date_created')[0]

	diff_list = []
	for versionSG, versionAdminApproved in zip(sg.version_set.order_by('content_type','object_id'), latest_admin_approved.revision.version_set.order_by('content_type','object_id')):
		# get the old and the new value
		for field in versionSG.field_dict:
			new_value = versionSG.field_dict.get(field)
			old_value = versionAdminApproved.field_dict.get(field)

			# lists might get out of order
			try:
				new_value = str(new_value.sort())
				old_value = str(old_value.sort())
			except:
				pass
			if field not in {'edit_date', 'id'} and new_value != old_value:
				diff_list.append((versionSG.object, field, new_value, old_value))
				#raise Exception
				

	url_this_version = "/admin/search/company/%s/history/%s/"%(company.pk, comp_finder(sg.version_set.all()).pk)
	url_approved_version = "/admin/search/company/%s/history/%s/"%(company.pk, latest_admin_approved.pk)

	hide_reject = (sg.versionapprove_set.all()[0].is_approved == 1)

	args = {'title':sg.comment, 'url_this_version':url_this_version, 'url_approved_version':url_approved_version, 'hide_reject':hide_reject, 'diffs':diff_list}
	args.update(extra_context)

	return render_to_response('admintools/suggestions_check.html', args, context_instance=RequestContext(request))


@csrf_protect
@login_required
def suggestions_approve(request, sg_pk, extra_context={}):

	user = request.user

	if not user.is_authenticated() or not user.is_staff:
		raise Http404

	sg = get_object_or_404(reversion.models.Revision, pk=sg_pk)
	# revision.add_meta(VersionApprove, is_approved=1)

	va = sg.versionapprove_set.all()[0]
	va.is_approved = 1
	va.save()
	sg.revert()

	return HttpResponseRedirect('/admintools/suggestions/')


@csrf_protect
@login_required
def suggestions_reject(request, sg_pk, extra_context={}):

	user = request.user

	if not user.is_authenticated() or not user.is_staff:
		raise Http404

	sg = get_object_or_404(reversion.models.Revision, pk=sg_pk)
	# revision.add_meta(VersionApprove, is_approved=1)

	va = sg.versionapprove_set.all()[0]
	va.is_approved = -1
	va.save()

	return HttpResponseRedirect('/admintools/suggestions/')


