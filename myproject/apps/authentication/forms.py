from django import forms
from models import *
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields=('first_name', 'last_name', )

		widgets={'first_name': forms.widgets.TextInput(attrs={'class': 'span4', 'placeholder':'first name'}),
				 'last_name': forms.widgets.TextInput(attrs={'class': 'span5', 'placeholder':'last name', 'style':'margin-left: 14px;'}),}



class UserProfileForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		exclude = ('user', 'editable_companies', 'approved_edits', 'is_company_user', 
					'daily_edit_cap', 'num_of_today_edits', 'num_of_all_edits', 'sponsership_tier',
					'email_prefs', 'sponsership_tier', )

		widgets={'location': forms.widgets.Select(attrs={'class': 'span9'}),
			'occupation': forms.widgets.SelectMultiple(attrs={'class': 'span9'}),
			'tracked_companies': forms.widgets.SelectMultiple(attrs={'class': 'span9'}),
			#'email_prefs': forms.widgets.SelectMultiple(attrs={'class': 'span12'}),
			'looking_for_commodities': forms.widgets.SelectMultiple(attrs={'class': 'span9'}),
			'gender': forms.widgets.Select(attrs={'class': 'not-select2'}),
			#'sponsership_tier': forms.widgets.Select(attrs={'class': 'not-select2'}),
			'type_of_investor': forms.widgets.SelectMultiple(attrs={'class': 'span9'}),
			'dob': forms.widgets.TextInput(attrs={'class': '', 'size':'16', 'type':'search'})
		}