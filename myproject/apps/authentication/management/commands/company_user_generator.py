from django.core.management.base import BaseCommand, CommandError, NoArgsCommand

from django.contrib.auth.models import User
from apps.search.models import Company
from apps.authentication.models import *
from django.template import Context, Template
from zlib import crc32



class Command(NoArgsCommand):
	def handle_noargs(self, **options):

		template_file = open('company_email.html')
		email_temp = Template(template_file.read())
		template_file.close()

		for company in Company.objects.all():

			if company.company_level == 0:
				try:
					username = "TSXV"+company.ticker_symbol
					password = str(crc32(company.ticker_symbol))
					user = User.objects.create_user(username, company.contactinfo.email, password)
					user.save()
					user.userprofile.is_company_user = company
					user.userprofile.editable_companies.add(company)
					user.save()

					subject = company.company_name+" is now on MiningSearch.ca"


					context = Context ({"company_name": company.company_name, "username": username, "password": password,} )

					rendered_email = email_temp.render(context)

					if company.contactinfo.email:
						user.email_user(subject, rendered_email, "registration@miningsearch.ca")
				
				except:
					pass


# fil = open('passwords.csv', 'w+')
# fil.write("NAME, USERNAME, PASSWORD \n")
# for company in Company.objects.all():
# 	name = company.company_name
# 	username = "TSXV"+company.ticker_symbol
# 	password = str(crc32(company.ticker_symbol))
# 	fil.write(name +", "+ username + ", " + password + "\n")


