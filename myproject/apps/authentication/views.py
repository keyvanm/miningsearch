from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth.models import User
from models import *
from apps.search.models import Company
from forms import *
from zlib import crc32

from apps.ad_interface.models import *


@csrf_protect
def portfolio(request, url_username, extra_context={}):
	user = get_object_or_404(User, username=url_username)

	try:
		portfolio = user.userprofile.tracked_companies.all()
	except:
		raise Http404

	ads_dict = {'results_broker_side_ad':single_ad_picker(6), 'results_client_side_ad':single_ad_picker(4)}

	rss = []
	for company in portfolio:
		rss.append(company.rss_news)

	args = {'rss':rss, 'companies': portfolio, 'title':"%s's portfolio"%user.username, }
	args.update(extra_context)
	args.update(ads_dict)

	return render_to_response('authentication/portfolio.html', args, context_instance=RequestContext(request))


@csrf_protect
def recovery(request, extra_context={}):
	if request.method == 'POST':
		username = request.POST.get('recusername', None)
		email = request.POST.get('recemail', None)

		print username, email

		try:
			u = User.objects.get(username=username)
		except:
			return render_to_response('registration/passrecover.html', {'errors': True}, context_instance=RequestContext(request))

		if u.email == email:
			new_pass = str(crc32(username + email))
			u.set_password(new_pass)
			email_body = "Hi %s\nYour MiningSearch password has been reset to %s. Please login at http://miningsearch.ca/ and change your password as soon as possible.\nThe MiningSearch Team"%(username, new_pass)
			u.email_user('Your MiningSearch.ca password has been reset', email_body, 'registration@miningsearch.ca')
			u.save()
			return HttpResponseRedirect('/')
		else:
			return render_to_response('registration/passrecover.html', {'errors': True}, context_instance=RequestContext(request))
	if request.method == 'GET':
		return render_to_response('registration/passrecover.html', {}, context_instance=RequestContext(request))




@csrf_protect
@login_required
def user_profile_edit(request, url_username, extra_context={}):
	
	user = request.user

	if user.username != url_username or not user.is_authenticated():
		raise Http404

	user_profile = user.userprofile

	if request.method == 'POST':
		user_form = UserForm(request.POST, instance=user)	
		profile_form = UserProfileForm (request.POST, instance=user_profile)

		if user_form.is_valid():
			user_form.save()
		if profile_form.is_valid():
			profile_form.save()

		return HttpResponseRedirect('/users/%s'%user.username)


	user_form = UserForm(instance=user)	
	profile_form = UserProfileForm (instance=user_profile)

	args = {'profile_form':profile_form, 'user_form':user_form, 'title':"%s's settings"%user.username, }
	args.update(extra_context)

	return render_to_response('authentication/user_profile_settings.html', args, context_instance=RequestContext(request))


@csrf_protect
@login_required
def user_profile_view(request, url_username, extra_context={}):

	this_user = get_object_or_404(User, username=url_username)
	
	user = request.user

	if (not user.is_authenticated() or user.username != url_username) and not user.is_staff and not user.is_superuser:
		raise Http404


	args = {'title':"%s's settings"%user.username, 'this_user':this_user}
	args.update(extra_context)

	return render_to_response('authentication/user_profile_view.html', args, context_instance=RequestContext(request))

