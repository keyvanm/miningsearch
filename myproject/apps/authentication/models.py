from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from django.contrib.auth import login
from registration import signals

from apps.search.models import Company, State, Commodity


class EmailPreferences(models.Model):

	preference_name = models.CharField(max_length=20)

	def __unicode__(self):
		return unicode(self.preference_name)

#type_of_investor class. Represents possible type_of_investors for the users.
class TypeOfInvestor(models.Model):
	type_of_investor_name = models.CharField(max_length=20)

	def __unicode__(self):
		return unicode(self.type_of_investor_name)
	

#The supplementary class for users. Stores the additional info.
class UserProfile(models.Model):
	# This field is required.
	user = models.OneToOneField(User)


	# additional info
	dob = models.DateField('Date of birth', blank=True, null=True)

	location = models.ForeignKey(State, blank=True, null=True)

	type_of_investor = models.ManyToManyField(TypeOfInvestor)

	GENDER_CHOICES = (
		('M', 'Male'),
		('F', 'Female'),
		('O', 'Other'),
	)

	gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True)


	# profile editing
	editable_companies = models.ManyToManyField(Company, blank=True, null=True, related_name='editable')

	daily_edit_cap = models.IntegerField(default=5)

	num_of_today_edits = models.IntegerField(default=0)

	  # if approved edits are over some number, increase daily_edit_cap
	num_of_all_edits = models.IntegerField(default=0)

	approved_edits = models.IntegerField(default=0)


	# sponsership
	USER_LVL_CHOICES = (
		(0, 'Non-Client'),
		(1, 'Client'),
		(2, 'Premium Client'),
	)
	sponsership_tier = models.SmallIntegerField('Sponsership Tier', choices = USER_LVL_CHOICES,
		default = 0)


	# settings
	looking_for_commodities = models.ManyToManyField(Commodity, blank=True, null=True)

	is_company_user = models.OneToOneField(Company, blank=True, null=True)

	tracked_companies = models.ManyToManyField(Company, blank=True, null=True, related_name='tracked')

	email_prefs = models.ManyToManyField(EmailPreferences, blank=True, null=True)

	def company_user_bool(self):
		if self.is_company_user == None:
			return 'False'
		return 'True'

	# company_user_bool.boolean = True

	def permission_for_realtime_edit(self, company):
		if (company == self.is_company_user) or (company in self.editable_companies.all()) or self.user.is_staff or self.user.is_superuser:
			return True
		return False



	def __unicode__(self):
		return unicode(self.user)

# automoatically make a user profile when a user is created
def create_user_profile(sender, **kwargs):
	"""When creating a new user, make a profile for him or her."""
	created = kwargs["created"]

	if created:
		u = kwargs["instance"]
		if not UserProfile.objects.filter(user=u):
			user_profile = UserProfile(user=u)
			user_profile.save()

			for email_pref in EmailPreferences.objects.all():
				user_profile.email_prefs.add(email_pref)

			user_profile.type_of_investor.add(TypeOfInvestor.objects.get_or_create(type_of_investor_name="Investor")[0])

post_save.connect(create_user_profile, sender=User)


# if a new type of email is added, add its pred to all users
def add_email_pref_to_user(sender, **kwargs):
	"""When creating a email_pref, add it to all users"""
	created = kwargs["created"]

	if created:
		pref = kwargs["instance"]
		for user in UserProfile.objects.all():
			user.email_prefs.add(pref)

post_save.connect(add_email_pref_to_user, sender=EmailPreferences)


# auto login after activation
def login_on_activation(user, request, **kwargs):
    user.backend='django.contrib.auth.backends.ModelBackend'
    login(request, user)

signals.user_activated.connect(login_on_activation)


