from models import *

from django.db import transaction
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.forms import (UserCreationForm, UserChangeForm,
	AdminPasswordChangeForm)
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.utils.html import escape
from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext, ugettext_lazy as _
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

csrf_protect_m = method_decorator(csrf_protect)



class EmailPreferencesAdmin(admin.ModelAdmin):
	search_fields = ('preference_name', )


class TypeOfInvestorAdmin(admin.ModelAdmin):
	search_fields = ('type_of_investor_name', )


# class UserInline(admin.TabularInline):
# 	model = User

from django.contrib.admin import SimpleListFilter

class CompUserListFilter(SimpleListFilter):
	# Human-readable title which will be displayed in the
	# right admin sidebar just above the filter options.
	title = _('Company User?')

	# Parameter for the filter that will be used in the URL query.
	parameter_name = 'company_user_bool'

	def lookups(self, request, model_admin):
		"""
		Returns a list of tuples. The first element in each
		tuple is the coded value for the option that will
		appear in the URL query. The second element is the
		human-readable name for the option that will appear
		in the right sidebar.
		"""
		return (
			('True', _('Yes')),
			('False', _('No')),
		)

	def queryset(self, request, queryset):
		"""
		Returns the filtered queryset based on the value
		provided in the query string and retrievable via
		`self.value()`.
		"""
		# Compare the requested value (either '80s' or 'other')
		# to decide how to filter the queryset.
		if self.value() == 'True':
			return queryset.filter(is_company_user__isnull = False)
		if self.value() == 'False':
			return queryset.filter(is_company_user__isnull = True)



class UserProfileAdmin(admin.ModelAdmin):
	#inlines = [UserInline]
	search_fields = ('user__username',)
	list_display = ('user', 'is_company_user')
	readonly_fields = ('dob', 'location', 'type_of_investor',
						'gender', 'looking_for_commodities', 'tracked_companies', 'email_prefs', )
	list_filter = (CompUserListFilter, )

	def get_readonly_fields(self, request, obj=None):
		if obj: # editing an existing object
			return self.readonly_fields + ('user', )
		return self.readonly_fields

	class Media:
		css = {
			 'all': ('stylesheets/select2.css', 'admin/select2-admin.css', )
		}
		js = ('javascript/jquery-1.8.3.min.js', 'javascript/select2.js', 'admin/select2-admin.js')

class UserAdmin(admin.ModelAdmin):
	add_form_template = 'admin/auth/user/add_form.html'
	change_user_password_template = None
	fieldsets = (
		(None, {'fields': ('username', 'password')}),
		(_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
		(_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
									   'groups', 'user_permissions')}),
		(_('Important dates'), {'fields': ('last_login', 'date_joined')}),
	)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('username', 'password1', 'password2')}
		),
	)

	def get_readonly_fields(self, request, obj=None):
		if request.user.is_superuser: # editing an existing object
			return []
		return ['username', 'first_name', 'last_name', 'email', 'is_staff',
				 'is_superuser', 'groups', 'user_permissions', 'last_login', 'date_joined']

	form = UserChangeForm
	add_form = UserCreationForm
	change_password_form = AdminPasswordChangeForm
	list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff')
	list_filter = ('is_staff', 'is_superuser', 'is_active')
	search_fields = ('username', 'first_name', 'last_name', 'email')
	ordering = ('username',)
	filter_horizontal = ('user_permissions',)

	def get_fieldsets(self, request, obj=None):
		if not obj:
			return self.add_fieldsets
		return super(UserAdmin, self).get_fieldsets(request, obj)

	def get_form(self, request, obj=None, **kwargs):
		"""
		Use special form during user creation
		"""
		defaults = {}
		if obj is None:
			defaults.update({
				'form': self.add_form,
				'fields': admin.util.flatten_fieldsets(self.add_fieldsets),
			})
		defaults.update(kwargs)
		return super(UserAdmin, self).get_form(request, obj, **defaults)

	def get_urls(self):
		from django.conf.urls import patterns
		return patterns('',
			(r'^(\d+)/password/$',
			 self.admin_site.admin_view(self.user_change_password))
		) + super(UserAdmin, self).get_urls()

	@sensitive_post_parameters()
	@csrf_protect_m
	@transaction.commit_on_success
	def add_view(self, request, form_url='', extra_context=None):
		# It's an error for a user to have add permission but NOT change
		# permission for users. If we allowed such users to add users, they
		# could create superusers, which would mean they would essentially have
		# the permission to change users. To avoid the problem entirely, we
		# disallow users from adding users if they don't have change
		# permission.
		if not self.has_change_permission(request):
			if self.has_add_permission(request) and settings.DEBUG:
				# Raise Http404 in debug mode so that the user gets a helpful
				# error message.
				raise Http404(
					'Your user does not have the "Change user" permission. In '
					'order to add users, Django requires that your user '
					'account have both the "Add user" and "Change user" '
					'permissions set.')
			raise PermissionDenied
		if extra_context is None:
			extra_context = {}
		defaults = {
			'auto_populated_fields': (),
			'username_help_text': self.model._meta.get_field('username').help_text,
		}
		extra_context.update(defaults)
		return super(UserAdmin, self).add_view(request, form_url,
											   extra_context)

	@sensitive_post_parameters()
	def user_change_password(self, request, id, form_url=''):
		if not self.has_change_permission(request):
			raise PermissionDenied
		user = get_object_or_404(self.queryset(request), pk=id)
		if request.method == 'POST':
			form = self.change_password_form(user, request.POST)
			if form.is_valid():
				form.save()
				msg = ugettext('Password changed successfully.')
				messages.success(request, msg)
				return HttpResponseRedirect('..')
		else:
			form = self.change_password_form(user)

		fieldsets = [(None, {'fields': form.base_fields.keys()})]
		adminForm = admin.helpers.AdminForm(form, fieldsets, {})

		context = {
			'title': _('Change password: %s') % escape(user.username),
			'adminForm': adminForm,
			'form_url': mark_safe(form_url),
			'form': form,
			'is_popup': '_popup' in request.REQUEST,
			'add': True,
			'change': False,
			'has_delete_permission': False,
			'has_change_permission': True,
			'has_absolute_url': False,
			'opts': self.model._meta,
			'original': user,
			'save_as': False,
			'show_save': True,
		}
		return TemplateResponse(request, [
			self.change_user_password_template or
			'admin/auth/user/change_password.html'
		], context, current_app=self.admin_site.name)

	def response_add(self, request, obj, post_url_continue='../%s/'):
		"""
		Determines the HttpResponse for the add_view stage. It mostly defers to
		its superclass implementation but is customized because the User model
		has a slightly different workflow.
		"""
		# We should allow further modification of the user just added i.e. the
		# 'Save' button should behave like the 'Save and continue editing'
		# button except in two scenarios:
		# * The user has pressed the 'Save and add another' button
		# * We are adding a user in a popup
		if '_addanother' not in request.POST and '_popup' not in request.POST:
			request.POST['_continue'] = 1
		return super(UserAdmin, self).response_add(request, obj,
												   post_url_continue)




admin.site.unregister(User)
admin.site.register(User, UserAdmin)



from registration.models import RegistrationProfile


class RegistrationAdmin(admin.ModelAdmin):
	actions = ['activate_users', 'resend_activation_email']
	list_display = ('user', 'activation_key_expired')
	# raw_id_fields = ['user']
	readonly_fields = ['user', 'activation_key']
	search_fields = ('user__username', 'user__first_name', 'user__last_name')

	def activate_users(self, request, queryset):
		"""
		Activates the selected users, if they are not alrady
		activated.

		"""
		for profile in queryset:
			RegistrationProfile.objects.activate_user(profile.activation_key)
	activate_users.short_description = _("Activate users")

	def resend_activation_email(self, request, queryset):
		"""
		Re-sends activation emails for the selected users.

		Note that this will *only* send activation emails for users
		who are eligible to activate; emails will not be sent to users
		whose activation keys have expired or who have already
		activated.

		"""
		if Site._meta.installed:
			site = Site.objects.get_current()
		else:
			site = RequestSite(request)

		for profile in queryset:
			if not profile.activation_key_expired():
				profile.send_activation_email(site)
	resend_activation_email.short_description = _("Re-send activation emails")

admin.site.unregister(RegistrationProfile)
admin.site.register(RegistrationProfile, RegistrationAdmin)




admin.site.register(EmailPreferences, EmailPreferencesAdmin)

admin.site.register(TypeOfInvestor, TypeOfInvestorAdmin)

admin.site.register(UserProfile, UserProfileAdmin)