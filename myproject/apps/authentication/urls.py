from django.conf.urls import patterns, include, url


urlpatterns = patterns('apps.authentication.views',
	url(r'^(?P<url_username>[^/]+)/portfolio/', 'portfolio'),
	url(r'^(?P<url_username>[^/]+)/settings/$', 'user_profile_edit'),
	url(r'^(?P<url_username>[^/]+)/$', 'user_profile_view'),
)