from django.conf.urls import patterns, include, url


urlpatterns = patterns('apps.company_profile_edit.views',
	url(r'^(?i)(?P<tk_symbol>[^/]+)/edit/submit/$', 'comp_edit'),
	url(r'^(?i)(?P<tk_symbol>[^/]+)/edit/$', 'comp_edit'),
	url(r'^(?i)(?P<tk_symbol>[^/]+)/track/$', 'comp_track'),
	url(r'^(?i)(?P<tk_symbol>[^/]+)/untrack/$', 'comp_untrack'),
	url(r'^(?i)(?P<tk_symbol>[^/]+)/$', 'comp_view'),
)