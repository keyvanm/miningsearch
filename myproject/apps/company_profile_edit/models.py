from django.db import models
import reversion
from django.db.models.signals import post_save
from apps.search.models import Company



class VersionApprove(models.Model):
    revision = models.ForeignKey('reversion.Revision')  # This is required

    is_approved_CHOICES = ((1, 'Approved'),
							(0, 'Pending'),
							(-1, 'Rejected'),)

    is_approved = models.SmallIntegerField(choices=is_approved_CHOICES, default=1)


def create_versionapprove(**kwargs):
	"""When creating a new user, make a profile for him or her."""
	# created = kwargs["created"]

	r = kwargs["revision"]
	if not VersionApprove.objects.filter(revision=r):
		VersionApprove(revision=r).save()

reversion.post_revision_commit.connect(create_versionapprove)



def comp_finder(instances):
	for i in instances:
		if isinstance(i, Company):
			return i
	return None

def auto_approve_versions(sender, **kwargs):
	pass
	versions = kwargs["versions"]
	revision = kwargs["revision"]
	instances = kwargs["instances"]
	company = instances[-1]
	if company == None:
		return

	if not revision.user.userprofile.permission_for_realtime_edit(company):
		# raise Exception
		latest_admin_approved = reversion.models.Version.objects.get_for_object(company).filter(revision__versionapprove__is_approved=1).order_by('-revision__date_created')[0]
		latest_admin_approved.revision.revert()
		# raise Exception("%s"%latest_admin_approved.revision.versionapprove.is_approved)


reversion.post_revision_commit.connect(auto_approve_versions)