from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from apps.search.models import *
from apps.company_profile_edit.forms import *

from reversion import revision
from models import VersionApprove
from reversion.models import Version
# from moderation.helpers import automoderate


from apps.ad_interface.models import *
import random


def automoderate(s1, s2):
	pass

# the navbar search form


@csrf_protect
def comp_view(request, tk_symbol, extra_context={}):
	#ads
	ads_dict = {'profile_side_broker_ad':single_ad_picker(7), 'profile_side_client_ad':single_ad_picker(3)}

	# get the company defined in the URL
	company = get_object_or_404(Company, ticker_symbol=tk_symbol.upper())

	tracking = company.tracked.count()
	# raise Exception

	rss = company.rss_news

	#get the company's list of properties, sort it
	# properties = company.property_set.order_by('pk')

	# the address of the company, converted to be used in google maps query
	gmaps_address = company.contactinfo.address.replace(' ', '+')

	args = {"rss": rss, "company": company, "gmaps_address": gmaps_address, "title": company.ticker_symbol, 'tracking':tracking}
	args.update(extra_context)

	args.update(ads_dict)
	# raise Exception
	return render_to_response('company_profile_edit/profile.html', args, context_instance=RequestContext(request))




#companyform, propertyforms, contactinfoform, managementforms, financialinfoform, socialform

# cross site request forgery protection
@login_required
@csrf_protect
@revision.create_on_success
def comp_edit(request, tk_symbol, extra_context={}):

	company = get_object_or_404(Company, ticker_symbol=tk_symbol.upper())

	# THE SUBMIT VIEW
	#If a POST request with edited info comess in, submit changes to DB.
	if request.method == 'POST':

		companyform = CompanyForm(request.POST, request.FILES, instance=company)
		# submit_errors = []

		if companyform.is_valid() and (request.FILES or companyform.has_changed()):
			companyform.save()
			automoderate(company, request.user)
		else:
			company.save()



		try:
			financialform = FinancialInfoForm(request.POST, instance=company.financialinfo)
		except:
			financialObject = FinancialInfo(company=company)
			financialObject.save()
			financialform = FinancialInfoForm(request.POST, instance=financialObject)

		if financialform.is_bound and financialform.is_valid() and financialform.has_changed()\
		 and financialform.cleaned_data.get('override_outstanding') and financialform.cleaned_data.get('outstanding'):
		 	company.financialinfo.market_cap = financialform.cleaned_data.get('outstanding') * company.financialinfo.price
			financialform.save()
			automoderate(company.financialinfo, request.user)
		# else:
		# 	for frm in financialform:
				# submit_errors.append((frm.label, frm.value, frm.errors))




		# the properties that were already defined
		for i, prop in enumerate(company.property_set.order_by('pk')):
			propform = PropertyForm(request.POST, instance=prop, prefix="property_set_%s_pk_%s" %(i, prop.pk))

			if propform.is_bound and propform.is_valid() and propform.has_changed():
				if propform.cleaned_data.get('deleteq', False):
					prop.delete()
				else:
					propform.save()
					automoderate(prop, request.user)
			# else:
			# 	for frm in propform:
					# submit_errors.append((frm.label, frm.value, frm.errors))


		# the new properties (dynamic)
		for i in range(int(request.POST.get('prop_new_count', 0))):
			add_prop = Property(company=company)
			add_propform = PropertyForm(request.POST, instance=add_prop, prefix="property_set_add_%s"%i)

			if add_propform.is_bound and add_propform.is_valid():
				if add_propform.cleaned_data.get('deleteq'):
					pass
				else:
					add_propform.save()
					automoderate(add_prop, request.user)
			# else:
			# 	for frm in add_propform:
					# submit_errors.append((frm.label, frm.value, frm.errors))


		# the officers that were already defined
		for i, officer in enumerate(company.officer_set.order_by('pk')):
			manageform = ManagementForm(request.POST, instance=officer, prefix="officer_set_%s_pk_%s" %(i, officer.pk))

			if manageform.is_bound and manageform.is_valid() and manageform.has_changed():
				# submit_errors.append(manageform.cleaned_data.get('deleteq'))
				if manageform.cleaned_data.get('deleteq'):
					officer.delete()
				else:
					manageform.save()
					automoderate(officer, request.user)
			# else:
			# 	for frm in manageform:
					# submit_errors.append((frm.label, frm.value, frm.errors))


		# the new officers (dynamic)
		for i in range(int(request.POST.get('officer_new_count', 0))):
			add_officer = Officer(company=company)
			add_officerform = ManagementForm(request.POST, instance=add_officer, prefix="officer_set_add_%s"%i)

			if add_officerform.is_bound and add_officerform.is_valid():
				# submit_errors.append(add_officerform.cleaned_data.get('deleteq'))
				if add_officerform.cleaned_data.get('deleteq'):
					pass
				else:
					add_officerform.save()
					automoderate(add_officer, request.user)
			# else:
			# 	for frm in add_officerform:
					# submit_errors.append((frm.label, frm.value, frm.errors))




		try:
			contactform = ContactInfoForm(request.POST, instance=company.contactinfo)
		except:
			contactObject = ContactInfo(company=company)
			contactform = ContactInfoForm(request.POST, instance=contactObject)

		if contactform.is_valid() and contactform.has_changed():
			contactform.save()
			automoderate(company.contactinfo, request.user)
		# else:
		# 	for frm in contactform:
				# submit_errors.append((frm.label, frm.value, frm.errors))


		try:
			socialform = SocialForm(request.POST, instance=company.socialmedia)
		except:
			socialmediaObject = SocialMedia(company=company)
			socialform = SocialForm(request.POST, instance=socialmediaObject)

		if socialform.is_valid() and socialform.has_changed():
			socialform.save()
			automoderate(company.socialmedia, request.user)
		# else:
		# 	for frm in socialmedia:
				# submit_errors.append((frm.label, frm.value, frm.errors))


		# response = ""
		# if submit_errors:
			# response = "Failed to update the following fields due to bad data input: " + str(submit_errors) + ". Other fields successfully submitted."
		# else:
		# 	response = "Successfully submitted.\n NEW PROPS:%s \n new officers: %s" %(request.POST.get('prop_new_count', 0), request.POST.get('officer_new_count', 0))

		# raise Exception
		# return HttpResponse(response)



		# revision control
		# revision.comment = "User with permission: "+request.POST.get('comment', '')
		# revision.add_meta(VersionApprove, is_approved=True)

		if request.user.userprofile.permission_for_realtime_edit(company):
			revision.add_meta(VersionApprove, is_approved=1)
			revision.comment = "User with permission: "+request.POST.get('comment', '')
		else:
			revision.add_meta(VersionApprove, is_approved=0)
			revision.comment = "Regular user: "+request.POST.get('comment', '')

			# latest_admin_approved = list(Version.objects.get_for_object(company).filter(revision__versionapprove__is_approved=True).order_by('revision__date_created'))[-1]
			# latest_admin_approved.revision.revert()


		return HttpResponseRedirect('/company/%s'%company.ticker_symbol)


	#ELSE show forms populated with data from DB.
	companyform = CompanyForm(instance=company)
	financialinfoform = FinancialInfoForm(instance=company.financialinfo)

	propertyforms = []
	for i, prop in enumerate(company.property_set.order_by('pk')): #propertyforms_add
		propertyforms.append(PropertyForm(instance=prop, prefix="property_set_%s_pk_%s" %(i, prop.pk)))
	# propertyforms_add = []
	propertyform_add = PropertyForm(prefix=r"property_set_add_%s")

	try:
		contactinfoform = ContactInfoForm(instance=company.contactinfo)
	except:
		ContactInfo(company=company).save()

	managementforms = []
	for i, officer in enumerate(company.officer_set.order_by('pk')):
		managementforms.append(ManagementForm(instance=officer, prefix="officer_set_%s_pk_%s" %(i, officer.pk)))
	# managementforms_add = []
	managementform_add = ManagementForm(prefix=r"officer_set_add_%s")
	try:
		socialform = SocialForm(instance=company.socialmedia)
	except:
		SocialMedia(company=company).save()



	args = {'company':company, 'contactinfoform':contactinfoform, 'managementforms':managementforms, 'socialform':socialform,\
	  'propertyforms':propertyforms, 'propertyform_add':propertyform_add, 'managementform_add':managementform_add,\
	    'companyform':companyform, 'financialinfoform':financialinfoform, 'company':company, 'title':'Edit %s'%company.ticker_symbol}

	args.update(extra_context);
	return render_to_response('company_profile_edit/edit.html', args, context_instance=RequestContext(request))



def comp_track(request, tk_symbol, extra_context={}):
	company = get_object_or_404(Company, ticker_symbol=tk_symbol)
	try:
		request.user.userprofile.tracked_companies.add(company)
		return HttpResponse()
	except:
		raise Http404


def comp_untrack(request, tk_symbol, extra_context={}):
	company = get_object_or_404(Company, ticker_symbol=tk_symbol)
	try:
		request.user.userprofile.tracked_companies.remove(company)
		return HttpResponse()
	except:
		raise Http404




