from django import forms
from apps.search.models import *

class CompanyForm(forms.ModelForm):
	class Meta:
		model = Company
		fields = ('company_name', 'company_desc', 'logo', 'rss_news', 'looking_for_commodities', )
		widgets = {'company_desc': forms.widgets.Textarea(attrs={'class': 'span12'}),
				   'rss_news': forms.widgets.TextInput(attrs={"class": 'span6'}),
				   'looking_for_commodities': forms.widgets.SelectMultiple(attrs={"class": 'span6'})}



class PropertyForm(forms.ModelForm):
	deleteq = forms.BooleanField(required=False)
	class Meta:
		model = Property
		exclude = ('company', )
		widgets = {'status': forms.widgets.Select(attrs={'class': 'not-select2'}),
					'ownership': forms.widgets.TextInput(attrs={'class': 'input-mini'}), 
					'option': forms.widgets.TextInput(attrs={'class': 'input-mini'}),
					'commodities': forms.widgets.SelectMultiple(attrs={'class': 'span12'}),
					'state': forms.widgets.Select(attrs={'class': 'span12'}),
					'deleteq': forms.widgets.CheckboxInput(attrs={'class': 'deleteq'}),
					}


# class PropertyNewForm(forms.ModelForm):
# 	class Meta:
# 		model = Property
# 		exclude = ('company', )
# 		widgets = {'name': forms.widgets.TextInput(attrs={'disabled': 'disabled'})}


class ContactInfoForm(forms.ModelForm):
	 class Meta:
		model = ContactInfo
		exclude = ('company', 'edit_date',)
		widgets = {'address': forms.widgets.Textarea(attrs={'class': 'span12', 'rows':'4'}),
				   'tel': forms.widgets.TextInput(attrs={'class': 'span12'}), 
				   'toll_free': forms.widgets.TextInput(attrs={'class': 'span12'}),
				   'fax': forms.widgets.TextInput(attrs={'class': 'span12'}), 
				   'email': forms.widgets.TextInput(attrs={'class': 'span12'}),  
				   'website': forms.widgets.TextInput(attrs={'class': 'span12'}), 
				   }


class ManagementForm(forms.ModelForm):
	deleteq = forms.BooleanField(required=False)
	class Meta:
		model = Officer
		exclude = ('company',)
		widgets = {'officer_name': forms.widgets.TextInput(attrs={'class': 'span12'}), 
					'positions': forms.widgets.SelectMultiple(attrs={'class': 'span12'}),
					'deleteq': forms.widgets.CheckboxInput(attrs={'class': 'deleteq'}),
					}


class FinancialInfoForm(forms.ModelForm):
	class Meta:
		model = FinancialInfo
		fields = ('outstanding', 'override_outstanding',)
		widgets = {'outstanding': forms.widgets.TextInput(attrs={'class': 'span12', 'disabled':'disabled', "pattern": "\d*", "data-validation-pattern-message":"Must be a number or decimal"}),}

class SocialForm(forms.ModelForm):
	class Meta:
		model = SocialMedia
		exclude = ('company',)
